import {
    FileSystem,
    Vibration
  } from "expo";
import * as ImagePicker from 'expo-image-picker'
import * as ImageManipulator from 'expo-image-manipulator'
import * as Permissions from 'expo-permissions'
import { AsyncStorage } from "react-native";
import api from "../utils/api";
import DropDownHolder from "../utils/alert";
import i18n from '../utils/i18n';
import moment from "moment";

async function takeAndUploadPhotoAsync() {
    await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL);
    let result = await ImagePicker.launchCameraAsync({
      allowsEditing: true,
      aspect: [4, 3]
    });
    let resizedPhoto = await ImageManipulator.manipulateAsync(
      result.uri,
      [
        {
          resize: {
            width: 600,
            height: (result.height / result.width) * 600
          }
        }
      ],
      {
        compress: 0.8,
        format: "jpeg",
        base64: false
      }
    );
    // FileSystem.moveAsync({
    //   from: resizedPhoto.uri,
    //   to: `${FileSystem.documentDirectory}photos/Photo_id.jpg`
    // });
    console.log("resized", resizedPhoto);

    if (result.cancelled) {
      return;
    } else {
        return resizedPhoto
    }
    
  }

async function uploadPhotoAsync() {
await Permissions.askAsync(Permissions.CAMERA_ROLL);
let result = await ImagePicker.launchImageLibraryAsync({
    allowsEditing: true,
    aspect: [4, 3]
});
let resizedPhoto = await ImageManipulator.manipulateAsync(
    result.uri,
    [
    {
        resize: {
        width: 600,
        height: (result.height / result.width) * 600
        }
    }
    ],
    {
    compress: 0.8,
    format: "jpeg",
    base64: false
    }
);
// FileSystem.moveAsync({
//   from: resizedPhoto.uri,
//   to: `${FileSystem.documentDirectory}photos/Photo_id.jpg`
// });
//console.log('result', result)
//console.log('resizedPhoto', resizedPhoto)
if (result.cancelled) {
    return;
} else {
    return resizedPhoto
}

}

async function deleteThing (thing) {
  const userToken = await AsyncStorage.getItem("userToken");
  return api
      .delete(`/things/${thing.id}`, {
        headers: {
          Accept: "application/json",
          "content-type": "",
          Authorization: "Bearer " + userToken}
        
      })

}

async function saveAll(thing) {
    //console.log('sortSERVICE SAVE', sort)
    console.log('thing service save', thing)
    // console.log("resultPOic", result);
    // ImagePicker saves the taken photo to disk and returns a local URI to it
    let localUri = thing.picture;
    console.log('localUri', localUri)
    let filename = localUri.split("/").pop();
    console.log('filename', filename)
    // Infer the type of the image
    let match = /\.(\w+)$/.exec(filename);
    let type = match ? `image/${match[1]}` : `image`;
    console.log('filename2', filename)
    // Upload the image using the fetch and FormData APIs
    let formData = new FormData();
    // Assume "photo" is the name of the form field the server expects
    console.log('filename3', filename)
    formData.append("photo", {
      uri: localUri,
      name: filename.split('.').slice(0, -1).join('.'),
      type
    });
    console.log('filename4', filename)
    // console.log("thisSave", this.props);
    console.log("toISOString()", thing.date);
    formData.append("name", thing.name);
    formData.append("description", thing.description);
    formData.append("typeId", thing.typeId);
    formData.append("categoryId", thing.categoryId);
    formData.append("radius", thing.radius);
    formData.append("reward", thing.reward);
    //formData.append("date", new Date(thing.date).toISOString())
    formData.append("date", moment(thing.date, "DDMMYYYY").toISOString() )
    const location = {
      latitude: thing.latitude,
      longitude: thing.longitude
    };
    formData.append("geo", JSON.stringify(thing.geo));
    formData.append("location", JSON.stringify(location));
    console.log("formData", formData);
    const idThing = thing.id;
    const userToken = await AsyncStorage.getItem("userToken");
    //console.log('things, token', userToken)
    if (!thing.id) {
      console.log('in')
      return api
      .post(`/things/upload`, formData, {
        headers: {
          Accept: "application/json",
          "content-type": "",
          Authorization: "Bearer " + userToken}
      })
    } else {
      return api
      .put(`/things/${idThing}/upload`, formData, {
        headers: {
          Accept: "application/json",
          "content-type": "",
          Authorization: "Bearer " + userToken}
      })
    }

  }

export {takeAndUploadPhotoAsync, uploadPhotoAsync, saveAll, deleteThing}