import {
    AsyncStorage,
  } from "react-native";
import api from "../utils/api";

async function getMyThings() {
    let self = this;
    //console.log('things')
    const userToken = await AsyncStorage.getItem("userToken");
    //console.log('things, token', userToken)
    try {
        return api
          .get("/things", {
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + userToken
            }
          })
      } catch (error) {
        console.error(error);
        const data =  error.response.data;
        DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message));
      }
}

export { getMyThings }
