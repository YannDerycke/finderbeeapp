import { Facebook } from "expo";
import api from "../utils/api";
import i18n from '../utils/i18n';
async function loginFB(pushToken) {
  try {
    console.log("pushToken in service", pushToken);
    // var x = {
    //   declinedPermissions: [],
    //   expires: 1562260046,
    //   permissions: ["public_profile", "email"],
    //   token:
    //     "EAAEZAjpHZAfCoBAJmHHlJeELWhcoNAi86pXZByMSqtaT76iKwTGuerQwshjQY8fTwNrBtBkgqFCE9jZCeAgZBNG08cHhsqUKe9lZC0kywSLI52UUiR7Ay6orZAF6oxYW4RTrVwGhCBDGZBHZCKpPZBnMgdX540MGpS4KNhTlCC9Efhkc0aEJ6W5x9WZCEqCjvXAeRPfKNfH2J8ly5ZBXQv738rA3JVHFGRRbZCjoZD",
    //   type: "success"
    // };

    const {
      type,
      token,
      expires,
      permissions,
      declinedPermissions
    } = await Facebook.logInWithReadPermissionsAsync("309575099710506", {
      permissions: ["public_profile", "email"]
    });
    console.log('accesTOken facebook', token)
    if (type === "success") {
      // Get the user's name using Facebook's Graph API
      console.log("tokenFacebook", token);
      return fetch(
        `https://graph.facebook.com/me?access_token=${token}&fields=id,birthday,name,email,first_name,last_name`,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          }
        }
      )
        .then(response => {
          return response.json();
        })
        .then(response => {
          console.log("lastR", response);
          const userInfos = { ...response, ...{ tokenFacebook: token } };
          userInfos.pushNotification = pushToken;
          userInfos.language = i18n.locale;
          console.log("userinfo", userInfos);
          if(response.error) {
            return new Promise(function(resolve, reject) {
                reject(response);
            });
          }
          return api.post("/users/loginFacebook", {
            userFacebook: userInfos
          }, {
            headers: {
              Accept: "application/json",
              "Content-type": "application/json"
            }
          })
          .then(response => {
            console.log("Jsw token for login facebook", JSON.stringify(response));
            return response;
              
          })
          .catch(error => {
            console.log('data',error)
          });
        });
    } else {
      // type === 'cancel'
    }
  } catch ({ message }) {
    alert(`Facebook Login Error: ${message}`);
  }
}
export default loginFB;
