import axios from "axios";
const url = {
  baseURL: "https://finder-bee-server.herokuapp.com",
  //baseURL: "http://192.168.1.48:3000",
  //baseURL: "http://localhost:3000", 10.110.28.35:19000
  //baseURL: "http://10.110.28.53:3000", 
  //baseURL: "http://10.110.28.39:3000",
  //baseURL: "http://10.110.28.24:3000",
  //baseURL: "http://10.110.28.56:3000",
  //baseURL: "http://10.110.28.45:3000",
  //baseURL: "http://10.110.29.70:3000",
  timeout: 4000
};
const $axios = axios.create(url);
export default $axios;
