import validation from 'validate.js'
import i18n from '../utils/i18n';

const constrains = {
    signUp: {
        name: {
            presence: {
                message:  "^" + i18n.t('fill'),
            },
        },
        surname: {
            presence: {
                message:  "^" + i18n.t('fill'),
            },
        },
        email: {
            presence: {
                message:  "^" + i18n.t('fill'),
            },
            format: {
                pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                message: () => {return "^" + i18n.t('invalidEmail')},
            }
        },
        password: {
            presence: {
                message:  "^" + i18n.t('fill'),
            },
            length: {
                minimum: 4,
                message: () => {return "^" + i18n.t('invalidPassword')},
            },
            
        },
        confirmPassword: {
            presence: {
                message:  "^" + i18n.t('fill'),
            },
            equality: 'password'
        },
        // phoneNo: {
        //     presence: true,
        //     format: {
        //         pattern: "^[0-9]{10}$",
        //         message: () => {return "^" + i18n.t('invalidPhone')},
        //     },
        // },
        
    },
    signIn: {
        email: {
            presence: {
                message:  "^" + i18n.t('fill'),
            },
            format: {
                pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                message: () => {return "^" + i18n.t('invalidEmail')},
            }
        },
        password: {
            presence: {
                message:  "^" + i18n.t('fill'),
            },
            length: {
                minimum: 4,
                message: () => {return "^" + i18n.t('fill')},
            },
            
        }
    },
    me: {
        name: {
            presence: {
                message:  "^" + i18n.t('fill'),
            },
            length: {
                minimum: 1,
                message: () => {return "^" + i18n.t('fill')},
            },
        },
        surname: {
            presence: {
                message:  "^" + i18n.t('fill'),
            },
            length: {
                minimum: 1,
                message: () => {return "^" + i18n.t('fill')},
            },
        },
        email: {
            presence: {
                message:  "^" + i18n.t('fill'),
            },
            format: {
                pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                message: () => {return "^" + i18n.t('invalidEmail')},
            }
        }
    },
    modifyPassword: {
        oldPassword: {
            presence: {
                message:  "^" + i18n.t('fill'),
            },
            length: {
                minimum: 4,
                message: () => {return "^" + i18n.t('invalidPassword')},
            },
            
        },
        password: {
            presence: {
                message:  "^" + i18n.t('fill'),
            },
            length: {
                minimum: 4,
                message: () => {return "^" + i18n.t('invalidPassword')},
            },
            
        },
        confirmPassword: {
            presence: {
                message:  "^" + i18n.t('fill'),
            },
            equality: 'password'
        },
        // phoneNo: {
        //     presence: true,
        //     format: {
        //         pattern: "^[0-9]{10}$",
        //         message: () => {return "^" + i18n.t('invalidPhone')},
        //     },
        // },
        
    },
    details: {
        name: {
            presence: {
                message:  "^" + i18n.t('name'),
            },
        },
        description: {
            presence: {
                message:  "^" + i18n.t('description'),
            },
        },
        picture: {
            presence: {
                message:  "^" + i18n.t('picture'),
            },
        },
        geo: {
            presence: {
                message:  "^" + i18n.t('geo'),
            },
        },
        reward: {
            presence: {
                message:  "^" + i18n.t('reward'),
            },
        },
        categoryId: {
            presence: {
                message:  "^" + i18n.t('category'),
            },
        },
        typeId: {
            presence: {
                message:  "^" + i18n.t('type'),
            },
        },

    }
}

export function validate(fieldName, value) {
    console.log('fieldName', fieldName)
    console.log('value', value)
    const formValues = {}
    formValues[fieldName] = value === "" ? undefined: value;

    const formFields = {}
    formFields[fieldName] = constraints[fieldName]

    // console.log('formValues', formValues)
    // console.log("formFields", formFields)
    const result = validation(formValues, formFields)
    if (result) {
	return result[fieldName][0]
    }
    return null
}

export function validateAll(userForm, type) {
   
    const result = validation(userForm, constrains[type])
    console.log('result', result)
    if (result) {
	return result
    }
    return null 
}

// export function comparePassword(password, confirmPassword) {
//     const result =  validation.single({password, confirmPassword}, constraints);
//     if (result) {
// 	return result
//     }
//     return null 
// }