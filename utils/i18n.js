import * as Localization from 'expo-localization';
import i18n from 'i18n-js';

import en from '../locale/en.json';
import fr from '../locale/fr.json';
import es from '../locale/es.json';

i18n.defaultLocale = Localization.locale;
i18n.locale = 'fr';
i18n.fallbacks = true;
i18n.translations = { en, fr, es };
//console.log('i18', i18n) 
export default i18n;