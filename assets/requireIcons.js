const images = {
    KEYS: require('./icons/key.png'),
    PILLS: require('./icons/pill.png'),
    SMARTPHONE: require('./icons/smartphone.png'),
    BAG: require('./icons/bag.png'),
    CREDIT: require('./icons/credit.png'),

}
    
export default images;