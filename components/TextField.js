import React from 'react'
import { View, TextInput, Text, StyleSheet } from 'react-native'
import validation from "../utils/validate";

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center"
    },
    input: {
      margin: 10,
      padding: 8,
      color: 'black',
      borderRadius: 14,
      fontSize: 18,
      fontWeight: '500',
      borderBottomColor: "#FF0066",
      borderBottomWidth:1

    },
    error: {
        marginLeft: 10,
        fontSize: 18,
        color:"red"
    }
  });

const TextField = props => (
  <View>
    {/* <Text style={styles.error}>{props.placeholder}</Text>  */}
    <TextInput 
        autoFocus = {props.autoFocus}
        placeholder={props.placeholder} 
        secureTextEntry={props.security}
        style={styles.input}
        onChangeText={(value) => props.onChangeText(props.name, value)}
        onBlur={() => props.onBlur(props.name)}
        keyboardType={props.keyboardType}
        value = {props.val} 
    /> 
    {/* <Text>val:{props.val}</Text>  */}
    <Text style={styles.error}>{props.error}</Text> 
  </View>
)

export default TextField
