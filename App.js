import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  SafeAreaView,
  StatusBar,
  AsyncStorage
} from "react-native";
import Navigation from "./navigation/Navigation";
import {
  Camera,
  Permissions,
  Notifications,
  FileSystem,
  Localization,
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  Linking
} from "expo";
import DropdownAlert from 'react-native-dropdownalert';
import DropDownHolder from "./utils/alert";
import Spinner from 'react-native-loading-spinner-overlay';
import i18n from './utils/i18n'; 

// const folder = `${FileSystem.cacheDirectory}expo-cache-example`;
// const en = {
//   foo: "Foo",
//   bar: "Bar {{someValue}}"
// };
// const fr = {
//   foo: "comme tu es fous",
//   bar: "chatouiller {{someValue}}"
// };


import { Provider } from "react-redux";
import Store from "./Store/configStore";

export default class 
 extends React.Component {
  state = {
    hasCameraPermission: null,
    type: Camera.Constants.Type.back,
    tit: "bhbhbhbb",
    spinner: false,
    iconTestPath:
      "file:///data/user/0/host.exp.exponent/cache/ExperienceData/%2540yanndentich%252Ffinderbee/Camera/96bb455d-5bac-4506-951b-fdaff5fb830a.jpg"
  };

  async componentDidMount() {
    const language = await AsyncStorage.getItem("language");
    console.log('languagedidmoun', language) 
    if( language) {
      // this.setState({language})
      i18n.locale = language; 
    }

    function select(state) {
      //console.log('state', state)
      return state.helpers.spinner
    }
    let currentValue
    function handleChange() {
      let previousValue = currentValue
      currentValue = select(Store.getState())
            
      if (previousValue !== currentValue) {
        this.setState({
          spinner: currentValue
        });
      }
    }

    const unsubscribe = Store.subscribe(handleChange.bind(this))
    //unsubscribe()
    // setInterval(() => {
    //   this.setState({
    //     spinner: !this.state.spinner
    //   });
    // }, 3000);
    console.log('this.Store', Store)
    const uriPrefix = Linking.makeUrl();
    console.log("utiPrefix", uriPrefix);
    const Networking = NativeModules.Networking;
    Networking.clearCookies(cleared => {
      console.debug("cleared hadCookies: " + cleared.toString());
    });
    // const { status } = await Permissions.askAsync(Permissions.CAMERA);
    // this.setState({ hasCameraPermission: status === "granted" });
  }

  async add() {
    // // Display an interstitial
    // AdMobInterstitial.setAdUnitID("ca-app-pub-3940256099942544/1033173712"); // Test ID, Replace with your-admob-unit-id
    // AdMobInterstitial.setTestDeviceID("EMULATOR");
    // await AdMobInterstitial.requestAdAsync();
    // await AdMobInterstitial.showAdAsync();
  }
  // static dropDown;
  // setDropDown(type, info, message) {
  //   console.log('nknknk', type, info, message)
  //   console.log('this.dropDownAlertRef',this.dropDownAlertRef)
  //   console.log('this', this)
  //   this.dropDownAlertRef.alertWithType(type, info, message);
  // }
  // static getDropDown() {
  //     return this.dropDown;
  // }
  render() {
    return (
      <Provider store={Store}>
        <StatusBar hidden={true}/>
        <SafeAreaView style={{flex: 1}} forceInset={{'top': 'never'}}>
          <Navigation />
          <Spinner
            visible={this.state.spinner}
            // textContent={'Loading...'}
            color={'#FF0066'}
            // animation={"slide"}
            textStyle={styles.spinnerTextStyle}
          />
          <DropdownAlert ref={(ref) => DropDownHolder.setDropDown(ref)}/>
        </SafeAreaView>

      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
