// Navigation/Navigation.js
import React from "react";
import { Ionicons } from "@expo/vector-icons";
import {
  Linking,
  Constants
} from "expo";
import { StyleSheet, Image, Button, Text, Icon } from "react-native";
import {
  createStackNavigator,
  createAppContainer,
  createMaterialTopTabNavigator,
  createDrawerNavigator,
  createSwitchNavigator,
  createBottomTabNavigator
} from "react-navigation";
import { fromLeft, zoomIn, zoomOut } from 'react-navigation-transitions'
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import AuthLoadingScreen from "../screens/AuthLoadingScreen";
import SignUpScreen from "../screens/SignUpScreen";
import SignInScreen from "../screens/SignInScreen";
import AuthScreen from "../screens/AuthScreen";
import LogOutScreen from "../screens/LogoutScreen";
import HomeScreen from "../screens/HomeScreen";
import AddMobScreen from "../screens/AddMobScreen";
import PushNotificationScreen from "../screens/PushNotification";
import NotificationsScreen from "../screens/NotificationsScreen";
import MapScreen from "../screens/MapScreen";
import TakePictureScreen from "../screens/TakePictureScreen";
import SearchScreen from "../screens/SearchScreen";
import FormObjectScreen from "../screens/FormScreen";
import MyObjectsScreen from "../screens/MyObjectsScreen";
import ListScreen from "../screens/ListScreen";
import SwiperFormScreen from "../screens/SwiperFormScreen";
import DetailsScreen from "../screens/DetailsScreen";
import AddThingScreen from "../screens/AddThingScreen";
import FormScreen from "../screens/FormScreen"; 
import UserScreen from "../screens/UserScreen"; ChangePasswordScreen
import ChangePasswordScreen from "../screens/ChangePasswordScreen";
import i18n from '../utils/i18n';

const baseNavigationOptions = {
  headerStyle: { backgroundColor: "red" },
  headerTitleStyle: { color: "white" }
};
const UserStack = createStackNavigator({
  // userProfile: {
  //   screen: UserScreen,
  //   navigationOptions: () => ({
  //     header: null
  //     // title: "FinderBee",
  //     // headerTintColor: "black",
  //     // headerStyle: {
  //     //   backgroundColor: "white"
  //     // },
  //     // headerTitleStyle: {
  //     //   fontWeight: "bold"
  //     // }
  //   })
  // },
  // passwordScreen: {
  //   screen: ChangePasswordScreen,
  //   navigationOptions: () => ({
  //     header: null
  //   })
  // },
  UserScreen: {
    screen: UserScreen,
    navigationOptions: () => ({
      title: i18n.t('navigation.myInfos'),
      headerTintColor: "black",
      headerStyle: {
        backgroundColor: "white"
      }
    })
  },
  passwordScreen: {
    screen: ChangePasswordScreen,
    navigationOptions: () => ({
      title: i18n.t('navigation.modifyPassword'),
      headerTintColor: "black",
      headerStyle: {
        backgroundColor: "white"
      }
    })
  },
})
const HomeStack = createStackNavigator({
  Screen1: {
    screen: HomeScreen,
    navigationOptions: () => ({
      title: "FinderBee",
      headerTintColor: "black",
      headerStyle: {
        backgroundColor: "white"
      },
      headerTitleStyle: {
        fontWeight: "bold"
      }
    })
  },
  MyObjectsScreen: {
    screen: MyObjectsScreen,
    navigationOptions: () => ({
      title: i18n.t('navigation.myThings'),
      headerTintColor: "black",
      headerStyle: {
        backgroundColor: "white"
      }
    })
  },
  // UserScreen: {
  //   screen: UserScreen,
  //   navigationOptions: () => ({
  //     title: i18n.t('navigation.myInfos'),
  //     headerTintColor: "black",
  //     headerStyle: {
  //       backgroundColor: "white"
  //     }
  //   })
  // },
  // passwordScreen: {
  //   screen: ChangePasswordScreen,
  //   navigationOptions: () => ({
  //     title: i18n.t('navigation.modifyPassword'),
  //     headerTintColor: "black",
  //     headerStyle: {
  //       backgroundColor: "white"
  //     }
  //   })
  // },
  AddScreen: {
    screen: AddThingScreen,
    navigationOptions: () => ({
      title: i18n.t('navigation.update'),
      headerTintColor: "black",
      headerStyle: {
        backgroundColor: "white"
      },
      headerTitleStyle: {
        fontWeight: "bold"
      }
    })
  },
  

});

HomeStack.navigationOptions = ({ navigation }) => {   
  let tabBarVisible = true;   
  if (navigation.state.index > 0) { tabBarVisible = false; }
   return {tabBarVisible}; 
}; 

const PushNotificationStack = createStackNavigator({
    Screen1: {
      screen: PushNotificationScreen,
      navigationOptions: () => ({
        title: "Push Notification",
        headerTintColor: "black",
        headerStyle: {
          backgroundColor: "white"
        }
      })
    }
  });

  const AddThingScreenStack = createStackNavigator(
    {
      AddThingScreens: {
        screen: AddThingScreen,
        navigationOptions: () => ({
          title: i18n.t('navigation.add'),
          headerTintColor: "black",
          headerStyle: {
            backgroundColor: "white"
          }
        }),
      },  
    },
  )

  const DetailsStack = createStackNavigator({

    DetailsScreen: {
      screen: DetailsScreen,
      navigationOptions: () => ({ 
        header: null,
      })
    }
  }, {mode: 'modal',
  headerMode: 'none',});
  const SearchScreenStack = createStackNavigator(
  {
    SearchScreen: {
      screen: SearchScreen,
      path:"search",
      navigationOptions: () => ({
        title: i18n.t('navigation.search'),
        headerTintColor: "black",
        headerStyle: {
          backgroundColor: "white"
        }
      })
    },
    ListScreen: {
      screen: ListScreen,
      path:"list",
      navigationOptions: () => ({
        title: i18n.t('navigation.list'),
        headerTintColor: "black",
        headerStyle: {
          backgroundColor: "white"
        }
      })
    },
    DetailsScreen: {
      screen: DetailsScreen,
      path:"details/:id",
      navigationOptions: {
        title: i18n.t('navigation.details'),
        headerTintColor: "black",
        headerStyle: {
          backgroundColor: "white"
        }
      }
    },
    UpdateThingScreen: {
      screen: AddThingScreen,
      navigationOptions: {
        title: "UpdateThingScreen",
        headerTintColor: "black",
        headerStyle: {
          backgroundColor: "white"
        }
      }
    }
  },   
  {
    transitionConfig: () => fromLeft(),
  });
  SearchScreenStack.navigationOptions = ({ navigation }) => {   
    let tabBarVisible = true;   
    if (navigation.state.index > 0) { tabBarVisible = false; }
     return {tabBarVisible}; 
  };
const AddMobStack = createStackNavigator({
  Screen1: {
    screen: AddMobScreen,
    navigationOptions: () => ({
      title: "AddMob",
      headerTintColor: "white",
      headerStyle: {
        backgroundColor: "red"
      }
    })
  }
});

const NotificationsStack = createStackNavigator({
  NotificationsScreen: {
    screen: NotificationsScreen,
    navigationOptions: () => ({
      header: null
    })
  },
  DetailsScreen: {
    screen: DetailsScreen,
    navigationOptions: () => ({
      title: i18n.t('navigation.details'),
      headerTintColor: "black",
      headerStyle: {
        backgroundColor: "white",
        headerStyle: { marginTop: Constants.statusBarHeight }
      }
    })
  }
});

const TabScreen = createMaterialTopTabNavigator(
  {
    NotificationScreen: { 
      screen: NotificationsStack,
      path:"notifications",
      navigationOptions: { title: i18n.t('navigation.notifications') }
    },
    PushNotificationScreen: { 
      screen: PushNotificationScreen,
      path:"messages",
      navigationOptions: { title: i18n.t('navigation.messages') }
    },
  },
  {
    tabBarPosition: 'top',
    swipeEnabled: true,
    animationEnabled: true,
    tabBarPosition:'top',
    tabBarOptions: {
      activeTintColor: '#FFFFFF',
      inactiveTintColor: '#F8F8F8',
      style: {
        backgroundColor: '#FF0066',
      },
      labelStyle: {
        textAlign: 'center',
      },
      indicatorStyle: {
        borderBottomColor: 'red',
        borderBottomWidth: 2,
      },
    },
  }
);
const cust = createMaterialBottomTabNavigator(
  {
    HomeScreen: {
      screen: HomeStack,
      navigationOptions: {
        title: "My Plans",
        tabBarColor: "white",
        tabBarIcon: ({ focused }) => (
          <Ionicons
            color={focused ? "#FF0066" : "grey"}
            name="md-home"
            // name={focused ? "ios-home" : "md-home"}
            size={25}
          />
        ),
        tabBarOnPress: ({navigation,defaultHandler}) => { 
          defaultHandler()
          // // console.log('nav', navigation)
          // // console.log('defaultHandler', defaultHandler)
          // if(navigation.state.index !==0) {
          //   // navigation.dismiss()
          //   navigation.dispatch(navigation.actions.popToTop());
            
          //   //navigation.dispatch(resetAction);
          // } else {
          //   defaultHandler() 
          // }

        }
      }
    },
    SearchScreenTab: {
      screen: SearchScreenStack,
      path:"searchTab",
      navigationOptions: {
        title: "Searchsss",
        tabBarColor: "white",
        tabBarIcon: ({ focused }) => (
          <Ionicons
            name="md-search"
            color={focused ? "#FF0066" : "grey"}
            // name={focused ? "ios-list" : "ios-list-box"}
            size={25}
          />
        ),
        tabBarOnPress: ({navigation,defaultHandler}) => { 
          defaultHandler()
          // // console.log('nav', navigation)
          // // console.log('defaultHandler', defaultHandler)

          // if(navigation.state.index !==0) {
          //   // navigation.dismiss()
          //   navigation.dispatch(navigation.actions.popToTop());

          //   // navigation.actions.reset({
          //   //   index: 1,
          //   //   actions: [navigation.actions.navigate({ routeName: 'SearchScreen' })],
          //   // })
            
          //   //navigation.dispatch(resetAction);
          // } else {
          //   defaultHandler() 
          // }

        }
      }
    },
    AddScreenTab: {
      screen: AddThingScreenStack,
      navigationOptions: {
        title: "Add",
        tabBarColor: "white",
        tabBarIcon: ({ focused }) => (
          <Ionicons
            name="ios-add-circle-outline"
            color={focused ? "#FF0066" : "grey"}
            // name={focused ? "ios-list" : "ios-list-box"}
            size={25}
          />
        ),
        tabBarOnPress: ({navigation,defaultHandler}) => { 

          defaultHandler()
          // // console.log('nav', navigation)
          // // console.log('defaultHandler', defaultHandler)
          // if(navigation.state.index !==0) {
          //   // navigation.dismiss()
          //   navigation.dispatch(navigation.actions.popToTop());
            
          //   //navigation.dispatch(resetAction);
          // } else {
          //   defaultHandler() 
          // }

        }
      }
    }, 
    NotificationScreen: {
      screen: TabScreen,
      path:"notificationTab",
      navigationOptions: {
        title: "notification",
        tabBarColor: "white",
        tabBarIcon: ({ focused }) => (
          <Ionicons
            name="md-notifications"
            color={focused ? "#FF0066" : "grey"}
            // name={focused ? "ios-list" : "ios-list-box"}
            size={25}
          />
        )
      }
    },
    UserScreen: {
      screen: UserStack,
      navigationOptions: {
        title: "Me",
        tabBarColor: "white",
        tabBarIcon: ({ focused }) => (
          <Ionicons
            name="ios-person"
            color={focused ? "#FF0066" : "grey"}
            // name={focused ? "ios-list" : "ios-list-box"}
            size={25}
          />
        )
      }
    }
  },
  {
    initialRouteName: "HomeScreen",
    backBehavior:"history",
    activeColor: "white",
    inactiveColor: "black",
    barStyle: { backgroundColor: "orange" },
  },
);
console.log('cust', cust)

const styles = StyleSheet.create({
  icon: {
    width: 24,
    height: 24
  }
});


const AuthStack = createStackNavigator(
  { AuthScreen: AuthScreen,
    SignIn: {
      screen: SignInScreen,
      navigationOptions: () => ({
        title: i18n.t('navigation.signIn')
      }),
    },
    SignUp: {
      screen: SignUpScreen,
      navigationOptions: () => ({
        title: i18n.t('navigation.signUp')
      }),
    }, 
});
const MySwitch = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: { screen: cust, path:""},
    Auth: AuthStack
  },
  {
    initialRouteName: "AuthLoading"
  }
);
const AppContainer = createAppContainer(MySwitch);
export default () => {
  const prefix = Linking.makeUrl('/');
  console.log(prefix);

  // if not using expo then prefix would simply be `swapi://`

  return <AppContainer uriPrefix={prefix} />;
};


