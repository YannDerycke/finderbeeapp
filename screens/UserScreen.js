import React, { Component } from "react";
import { connect } from "react-redux";
import {
  ScrollView,
  Text,
  TextInput, 
  View,
  Button,
  StyleSheet,
  AsyncStorage
} from "react-native";
import { Linking, LinearGradient, Notifications, AdMobBanner} from "expo";
import TextField from '../components/TextField'
import {validate, validateAll, comparePassword} from "../utils/validate";
import DropDownHolder from "../utils/alert";
import i18n from '../utils/i18n';
import api from "../utils/api";
// import App from "../App"



class UserScreen extends React.Component {
  // static navigationOptions = ({ navigation, navigationOptions }) => {
  //   const { params = {} } = navigation.state;
  //   return {
  //     headerTitle: params.title,
  //   };
  // };
  constructor(props) {
    super(props)
    //this.state = {user:  {verified:false}, errorForm: {name:"", surname:"", email:"", password:""}}
    this.state = {user:this.props.helpers.user, errorForm: {name:"", surname:"", email:"", password:""}}
  }
  componentDidMount() {  

    console.log('this.props', this.props.helpers)
    // const { navigation } = this.props;
    // console.log('nav', navigation)
    // navigation.setParams({ title: i18n.t('signUp') });
  }
  goBack = () => {
    this.props.navigation.navigate("HomeScreen");
  }
  async logOut() {
    await AsyncStorage.removeItem("userToken"); 
    this.props.navigation.navigate("AuthLoading");
    // const action = {
    //   type: "EMPTY_MY_THINGS",
    //   value: null
    // };
    // this.props.dispatch(action);
  }
  onChangeText = (property, value) => { 
    
    this.setState({ user: { ...this.state.user, [property]: value} });
  }
  validateInput = (property) => {
    // let errorValue;
    // errorValue = validate(property, this.state.user[property]);
    // this.setState({
    //   errorForm: {...this.state.errorForm, [property]: errorValue}
    // })
  }
  modify = async () => {
    const user = this.state.user; 
    user.pushnotification = this.props.helpers.pushToken
    console.log('user', user)
    const testForm = validateAll(user, 'me');
    console.log('testForm', testForm)
    this.setState({
      errorForm: testForm === null ? {} : testForm
    })
    let validForm = !testForm;
    // console.log('user', user)
    if(validForm) {
      const userToken = await AsyncStorage.getItem("userToken");
      api.put("/users/", user, {
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
          Authorization: "Bearer " + userToken
      }
      })
      .then(response => {
        console.log('responseq', JSON.stringify(response))
        DropDownHolder.getDropDown().alertWithType('success', i18n.t('message'), i18n.t('successInfos'));
        const data =  response.data;
        const action = {
          type: "USER",
          value: data
        };
        this.props.dispatch(action);
        this.props.navigation.navigate("HomeScreen");

      })
      .catch(error => {
        console.log('error', JSON.stringify(error))
        const data =  error.response.data;
        DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message))
      });
    }
  }
  render() {
    let self = this;
    function gotToHome() {
      self.props.navigation.navigate("HomeScreen");
    }
    return (
      <ScrollView style={{ padding: 20 }}>
        <TextField
          name='name'
          placeholder={i18n.t('form.name')}
          val = {this.state.user.name}
          // autoFocus={true}
          onChangeText={this.onChangeText}
          onBlur={this.validateInput}
          error={this.state.errorForm.name}/>
        <TextField
          name='surname' 
          placeholder={i18n.t('form.surName')}
          val = {this.state.user.surname}
          onChangeText={this.onChangeText}
          onBlur={this.validateInput}
         
          error={this.state.errorForm.surname}/>
        <TextField
          name='email'
          placeholder={i18n.t('form.email')}
          val = {this.state.user.email}
          onChangeText={this.onChangeText}
          onBlur={this.validateInput}
          error={this.state.errorForm.email}/>
        <Text>{this.state.user.id}</Text>
        <View style={{ margin: 7 }} />
        
        <Button title={i18n.t('validate')} color="#FF0066" onPress={() => this.modify()} />
        <Text style={styles.link}               
          onPress={() => this.props.navigation.navigate("passwordScreen", {myThings: true, force: false })}>
          {i18n.t('modifyPassword')}
        </Text> 
        <Text style={styles.link}               
          onPress={() => this.logOut()}>
          {i18n.t('logOut')}
        </Text> 
      </ScrollView>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  input: {
    backgroundColor: '#42A5F5',
    margin: 10,
    padding: 8,
    color: 'white',
    borderRadius: 14,
    fontSize: 18,
    fontWeight: '500',
  },
  link: {
    textDecorationLine:'underline',
    marginBottom: 10,
    marginTop:10
  }
});

const mapStateToProps = state => {
  //console.log("stateOverView", state);
  const { helpers } = state;
  return { helpers };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: action => {
      dispatch(action);
    }
  };
};



export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserScreen);
