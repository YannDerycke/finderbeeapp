// import React, { Component } from "react";
// import { Ionicons } from "@expo/vector-icons";
// import { connect } from "react-redux";
// import {
//   ScrollView,
//   Text,
//   TextInput,
//   View,
//   Button,
//   StyleSheet,
//   AsyncStorage,
//   Picker,
//   Header
// } from "react-native";
// import loginFB from "../utils/auth";
// import Swiper from "react-native-swiper";
// import TakePicture from "../screens/TakePictureScreen";
// import FormScreen from "../screens/FormScreen";
// import MapScreen from "../screens/MapScreen";
// import SavingStatusScreen from "../screens/SavingStatusScreen";

// class SwiperFormScreen extends React.Component {
//   state = {
//     selectedCategory: "js"
//   };

//   static navigationOptions = {
//     title: "Step1",
//     drawerIcon: ({ tintColor }) => (
//       <Ionicons name="md-add" size={32} color="orange" />
//     )
//   };
//   componentDidMount() {}

//   _onScrollBeginDrag(e) {
//     console.log("e", e);
//   }
//   render() {
//     return (
//       <View style={{ flex: 1 }}>
//         <View style={{ flex: 1 }}>
//           <Swiper
//             onScrollBeginDrag={this._onScrollBeginDrag.bind(this)}
//             style={styles.wrapper}
//             //                showsButtons
//             //                removeClippedSubviews={false}

//             dot={
//               <View
//                 style={{
//                   backgroundColor: "rgba(255,165,0,.1)",
//                   width: 13,
//                   height: 13,
//                   borderRadius: 7,
//                   marginLeft: 24,
//                   marginRight: 24
//                 }}
//               />
//             }
//             loop={false}
//             activeDot={
//               <View
//                 style={{
//                   backgroundColor: "orange",
//                   width: 13,
//                   height: 13,
//                   borderRadius: 7,
//                   marginLeft: 24,
//                   marginRight: 24
//                 }}
//               />
//             }
//             paginationStyle={{ bottom: 18 }}
//           >
//             <View style={styles.form}>
//               <FormScreen />
//             </View>
//             <View style={styles.map}>
//               <MapScreen />
//             </View>
//             <View style={styles.picture}>
//               <TakePicture />
//             </View>
//           </Swiper>
//         </View>
//         {/* <SavingStatusScreen /> */}
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     alignItems: "center",
//     justifyContent: "center"
//   },

//   dotStyle: {
//     backgroundColor: "white",
//     justifyContent: "space-between"
//   },
//   activeDotStyle: {
//     backgroundColor: "orange"
//   },
//   wrapper: {},
//   form: {
//     flex: 1,
//     alignItems: "center",
//     backgroundColor: "white"
//   },
//   map: {
//     flex: 1,
//     alignItems: "center",
//     backgroundColor: "white"
//   },
//   picture: {
//     flex: 1,
//     alignItems: "center",
//     backgroundColor: "white"
//   },
//   text: {
//     color: "#fff",
//     fontSize: 30,
//     fontWeight: "bold"
//   }
// });

// const mapStateToProps = state => {
//   return state;
// };

// const mapDispatchToProps = dispatch => {
//   return {
//     dispatch: action => {
//       dispatch(action);
//     }
//   };
// };
// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(SwiperFormScreen);
