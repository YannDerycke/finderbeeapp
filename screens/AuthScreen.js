import React, { Component } from "react";
import { connect } from "react-redux";
import { Ionicons } from "@expo/vector-icons";
import {
  ScrollView,
  Icon,
  Text,
  TextInput,
  View,
  Button,
  StyleSheet,
  AsyncStorage,
  Picker,
  Image, 
  Dimensions
} from "react-native";
import {
  Camera,
  Permissions,
  Notifications,
  FileSystem,
  Localization,
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  Linking
} from "expo";
import i18n from '../utils/i18n';
import loginFB from "../utils/auth";
import DropDownHolder from "../utils/alert";

class AuthScreen extends React.Component {
  static navigationOptions = {
    title: "Please sign in SIGNIcN",
    header: null
  };
  constructor(props) {
    super(props);
    

    // this.props.navigation.navigate("AddMobScreen", { idFilm: "idFilm" });
  }
  state = {
    language:"en"
  };

  async componentDidMount() { 
    console.log('i18n.locale', i18n.locale)
    this.registerForPushNotificationsAsync()
    const language = await AsyncStorage.getItem("language");
    console.log('languagedidmount', language) 
    if( language) {
      this.setState({language})
      i18n.locale = language; 
    }
    
  }
  async changeLanguage (language) {
    console.log('auth', language)
    // console.log('lang', language)
    i18n.locale = language;
    // this.setState({language: language})
    this.setState({ language });
    await AsyncStorage.setItem("language", language);
  }
  async registerForPushNotificationsAsync() {
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );
    console.log("exist", existingStatus);
    let finalStatus = existingStatus;

    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== "granted") {
      // Android remote notification permissions are granted during the app
      // install, so this will only ask on iOS
      const { status } = await Permissions.askAsync(
        Permissions.NOTIFICATIONS
      );
      finalStatus = status;
    }

    // Stop here if the user did not grant permissions
    if (finalStatus !== "granted") {
      return;
    }

    // Get the token that uniquely identifies this device
    try {
      let pushToken = await Notifications.getExpoPushTokenAsync();
      console.log('token Permissions', pushToken)
      const action = {
        type: "PUSH_TOKEN",
        value: pushToken
      };
      this.props.dispatch(action);
      this.setState({pushToken})
    } catch(err) {
      console.log(err); // TypeError: failed to fetch
    }
  }

  loginFacebook() {
      
    loginFB(this.state.pushToken).then(response => {
      //console.log('res fb', JSON.stringify(response))
      const data = response.data;
      console.log('data', data)
      if (data.user) {
        console.log('datUSer', data.user)
        AsyncStorage.setItem("userToken", data.token);
        this.props.navigation.navigate("HomeScreen");
      }
    }).catch((error)=> {
      console.log('error  rr',error)
      DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), error.error.message);
    });
  }
  render() {
    const self = this;
    

    const dimensions = Dimensions.get('window');
    const imageHeight = Math.round(dimensions.width * 9 / 16);
    const imageWidth = dimensions.width - 20;
    return (
        <View style={{flex:1, flexDirection:'column',justifyContent:'space-between', padding:10}}>
          <View style={{flex:1, flexDirection:'row'}}>

            <View style={{flex:1, flexDirection:'row'}}>
              <View style={{flex:.1}}>
                <Ionicons
                    color={"black"}
                    name="ios-globe"
                    size={40}
                  />
              </View>

              <View style={{flex:.4}}>
                <Picker style={{flex:1}}  selectedValue = {this.state.language}  onValueChange = {(value) => {this.changeLanguage(value)}}>
                  <Picker.Item label="Français" value="fr"/>
                  <Picker.Item label="English" value="en"/>
                  <Picker.Item label="Español" value="es"/>
                </Picker>     
              </View> 

            </View>
          </View> 
          <View style={{flex:4, flexDirection:'column'}}>
            <Image
                style={{width: imageWidth}}
                source={require('../assets/finderBee.png')}
              />
          </View>
          <View style={
            {
              flex:1, 
              flexDirection:'column',  
              justifyContent:'center', 
              alignSelf:"center",
              
            }
            
          }>
            <Text style={styles.title}>
              {i18n.t('home.title')}
            </Text>
          </View>
          <View style={{flex:1, flexDirection:'column', justifyContent:'space-between'}}>
            <Button style={[styles.button, {backgroundColor: '#3b5998'}]} color="#3b5998" title={i18n.t('signFacebook')} onPress={() => this.loginFacebook()} />
          </View>
          <View style={{flex:.5, flexDirection:'row', justifyContent:'space-around'}}>
            {/* <Button
              style={{flex:1}}
              title={i18n.t('signUp')}
              onPress={() => this.props.navigation.navigate("SignUp")} 
            /> */}
                        <Text 
              style={{flex:1,  backgroundColor: 'white', textAlign:"center"}} 
             
              onPress={() => this.props.navigation.navigate("SignUp")} > {i18n.t('signUp')} </Text>
            
            <Text 
              style={{flex:1,  backgroundColor: 'white', textAlign:"center"}} 
             
              onPress={() => this.props.navigation.navigate("SignIn")} > {i18n.t('signIn')} </Text>
            {/* <Text style={styles.welcome}>
              {i18n.t('home.welcome', { appName: i18n.t('appName')})}
            </Text> */}
          </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    backgroundColor: '#42A5F5',
    margin: 10,
    padding: 8,
    color: 'white',
    borderRadius: 14,
    fontSize: 18,
    fontWeight: '500',
  },
  button: {
    backgroundColor: '#3b5998',
    margin: 10,
    padding: 8,
    color: 'red',
    borderRadius: 14,
    fontSize: 18,
    fontWeight: '500',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 20,
    textAlign: "center"
  }
});

const mapStateToProps = state => {
  //console.log("stateOverView", state);
  const { helpers } = state;
  return { helpers };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: action => {
      dispatch(action);
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthScreen);
