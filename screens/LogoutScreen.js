import React from "react";
import { Text, View, Button } from "react-native";

class LogOutScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Text style={{ fontSize: 30 }}>Do you really want to logout ?</Text>
        <Button
          onPress={() => this.props.navigation.navigate("AuthLoading")}
          title="Agree"
        />
        <Button
          onPress={() => this.props.navigation.navigate("Home")}
          title="Cancel"
        />
      </View>
    );
  }
}

export default LogOutScreen;
