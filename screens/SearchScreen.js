import React from "react";
import { connect } from "react-redux";
import { Ionicons } from "@expo/vector-icons";
import { Picker, 
    AsyncStorage, 
    Text,
    View, 
    Button, 
    Image, 
    Icon,
    StyleSheet, 
    Dimensions, 
    Alert, 
    TextInput, 
    KeyboardAvoidingView,
    Platform,
    Slider,
    TouchableHighlight} from "react-native";
import { 
    Linking, 
    Constants,
    Location,
    Permissions,
    LinearGradient,
    AdMobRewarded,
    AdMobInterstitial,
    Marker,
    Circle} from "expo";
import api from "../utils/api";
import MapView from 'react-native-maps';

import keys from "../assets/keys.png";
import Images from '../assets/requireIcons';
import FilterModal from "./FilterModalScreen";

import { StackNavigator } from "react-navigation";
import DatePicker from 'react-native-datepicker';
import moment from "moment";
import DropDownHolder from "../utils/alert";
import i18n from '../utils/i18n';

import {takeAndUploadPhotoAsync, uploadPhotoAsync, saveAll} from "../services/takePictures"

class SearchScreen extends React.Component {
  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params } = navigation.state;
    const dimensions = Dimensions.get('window');
    // return {
    //   header: <Header {...navigation} {...navigationOptions} />
    // };
    return {
      
      //header: null,
      title: i18n.t('navigation.search'),
      /* These values are used instead of the shared configuration! */
      headerStyle: {
        backgroundColor: "green",
        height:150
      },
      headerRight: (
        <View>
          {/* <View style={{flex:1, flexDirection:'row'}}>
            <Text style={{ flex:1 }}>Category</Text>
            <Text style={{ flex:1 }}>Type</Text>
          </View> */}
          <View style={{flex:1, flexDirection:'row', justifyContent:'flex-end', alignContent:'flex-end'}}>
          <Ionicons
              color={"#FF0066"}
              name="ios-funnel"
              style={{marginRight:5}}
              onPress={() => params.showFilter()}
              size={40}
            />
            {/* {params && params.categories ?
            <Picker
                selectedValue={params.categoryId}
                style={{ width: dimensions.width/3 }}
                onValueChange={params.selectCategory}>
                {params.categories.map(category => {
                    return <Picker.Item key={category.id} label={category.name} value={category.id} />
                })}
            </Picker> : <Text>fdsfds</Text>}
            {params && params.types ?
              <Picker
                  selectedValue={params.typeId}
                  style={{  width: dimensions.width/3 }}
                  onValueChange={params.selectType}>
                  {params.types.map(type => {
                      return <Picker.Item key={type.id} label={type.name} value={type.id} />
                  })}
              </Picker> : <Text>fdsfds</Text>} */}
          </View>
        </View>
      )
    };
  };

  constructor(props) {
    super(props);
  }

  state = {
    picture: {},
    modalVisible: true,
    categories: [],
    markers: [],
    radius: 10,
    infosLocation: {},
    categories: [],
    types: [],
    typeFilter: "",
    categoryFilter:"",
    filterModalVisible: false,
    fromDate: null,
    toDate: null,
    //searchToDate: moment().format('DD-MM-YYYY');
  }


  selectCategory = (categoryId) => {
    this.setState({categoryFilter: categoryId})
    const action = {
      type: "FILTER_CATEGORY",
      value: categoryId
    };
    this.props.dispatch(action); 
    //this.getObjects()
  };

  selectType = (typeId) => {
    this.setState({typeFilter: typeId})
    const action = {
      type: "FILTER_TYPE",
      value: typeId
    };
    this.props.dispatch(action);
    //this.getObjects()
  };

  changeFromDate = (date) => {
    console.log('chanfrom', date)
    this.setState({fromDate: date})
    if(date){
      this.setState({searchFromDate: moment(date, "DD-MM-YYYY").format('YYYY-MM-DD')})
    } else {
      this.setState({searchFromDate: null})
    }
  }
  changeToDate = (date) => {
    console.log('chanto', date)
    console.log('chanto2', moment(date, "DD-MM-YYYY").format('YYYY-MM-DD'))
    this.setState({toDate: date})
    if(date){
      this.setState({searchToDate: moment(date, "DD-MM-YYYY").format('YYYY-MM-DD')})
    } else {
      this.setState({searchToDate: null})
    }
  }

  showFilter() {
    this.setState({filterModalVisible: true})
  }
  closeFilter() {
    this.setState({filterModalVisible: false})
    this.getObjects()
  }

  async getObjects(infosLocation) {
    const action = {
        type: "SPINNER",
        value: true
    };
    this.props.dispatch(action);
    let self = this;
    //console.log('things')
    const userToken = await AsyncStorage.getItem("userToken");
    //console.log('things, token', userToken)
    try {
        // let latitude = this.state.infosLocation.latitude;
        // let longitude = this.state.infosLocation.longitude;
        let {latitude, longitude, latitudeDelta, longitudeDelta} = this.state.infosLocation
        function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
          var R = 6371; // km
          var dLat = toRad(lat2-lat1);
          var dLon = toRad(lon2-lon1);
          var lat1 = toRad(lat1);
          var lat2 = toRad(lat2);
    
          var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
          var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
          var d = R * c;
          return d*1000/2;
          }
          
          function toRad(Value) {
            return Value * Math.PI / 180;
          }
          let distance = getDistanceFromLatLonInKm(latitude, longitude,(latitude + latitudeDelta), (longitude + longitudeDelta) )
        console.log('dis', distance)
        //console.log('this', this.state)
      api
        .get("/things/find", {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + userToken
          },
          params: {
            long: longitude,
            lat: latitude,
            category: this.state.categoryFilter,
            type: this.state.typeFilter,
            distance,
            fromDate: this.state.searchFromDate,
            toDate: this.state.searchToDate

          }
        })
        .then(response => {
          //console.log("response.data", response.data[0]);
            const things = response.data;
            //console.log('things', things)
            this.setState({markers: []});
            things.forEach(thing => {
                this.addMarker(thing)
            });
            //console.log('length', this.state.markers.length)
            this.props.dispatch({
              type: "LIST_THINGS",
              value: {
                things, 
                categoryFilter: this.state.categoryFilter,
                typeFilter: this.state.typeFilter
              }
            });

            this.props.dispatch({
              type: "SPINNER",
              value: false
            });
            //this.switch(this.state.filterId);
            self.setState(
                { loading:false,
                  refreshing: false,
                  things,
                  countFiltered: things.length
                },
                function(e) {
                    //console.log("callback set", e);
                }
            );
        })
        .catch(error => {
          const action = {
              type: "SPINNER",
              value: false
          };
          this.props.dispatch(action);
          console.log(error);
          const data =  error.response.data;
          DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(('unknownError')));
        });
    } catch (error) {
      console.error(error);
      const data =  error.response.data;
      this.props.dispatch({
          type: "SPINNER",
          value: false
      });
      DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t('unknownError'));
    }
  }
  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      this.setState({
        errorMessage: "Permission to access location was denied"
      });
    }

    let location = await Location.getCurrentPositionAsync({
      enableHighAccuracy: true
    });
    //console.log('location', location)

    this.setState({
      region: {
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
        latitudeDelta: 0.01,
        longitudeDelta: 0.01
      } 
    });
  };
  onRegionChange = (infosLocation) => {
      //console.log('info', infosLocation)
      //this.getObjects(infosLocation)
      this.setState({
        infosLocation: infosLocation
      });
      //console.log('state', this.state)


  };
  addMarker(thing) {
    //const icon = require('../assets/icons/' + thing.category.name)
    //console.log('Images[thing.category.name]', Images[thing.category.name])
    this.setState({
      markers: [
        ...this.state.markers,
        { id: thing.id,
          image: Images[thing.category.name] ,
          title: thing.name,
          description: thing.description,
          coordinates: {
            latitude: thing.geo.coordinates[1],
            longitude: thing.geo.coordinates[0]
          }
        }
      ]
    });
  }
  componentWillUpdate() {
    console.log('ininininininininni')
    this.render()
  }
  async componentDidMount() {
        
      //this.getObjects();
      if (Platform.OS === "android" && !Constants.isDevice) {
        this.setState({
          errorMessage:
            "Oops, this will not work on Sketch in an Android emulator. Try it on your device!"
        });
        console.log('out')
      } else {
        console.log('in')
        this._getLocationAsync();
      }
      const categories = [...this.state.categories, ...this.props.helpers.categories];
      this.setState({categories: [...this.state.categories, ...this.props.helpers.categories]})
      // console.log('categ', categories)
      const types = [...this.state.types, ...this.props.helpers.types];
      this.setState({types: [...this.state.types, ...this.props.helpers.types]})
      this.props.navigation.setParams({ 
        selectCategory: this.selectCategory.bind(this), 
        selectType: this.selectType.bind(this), 
        categories, 
        types,
        showFilter: this.showFilter.bind(this)
      });
      AdMobInterstitial.setAdUnitID("ca-app-pub-3940256099942544/1033173712");
      AdMobInterstitial.removeAllListeners();

      AdMobInterstitial.addEventListener("interstitialDidLoad", () => {
        console.log("interstitialDidLoad");
        AdMobInterstitial.showAdAsync();
      });
  
      AdMobInterstitial.addEventListener("interstitialDidFailToLoad", () =>
        console.log("interstitialDidFailToLoad")
      );
  
      AdMobInterstitial.addEventListener("interstitialDidOpen", () => {
          console.log("interstitialDidOpen")
          this.props.navigation.navigate("DetailsScreen", {thing: this.state.thing})
        }
        
      );
      AdMobInterstitial.addEventListener("interstitialDidClose", () =>
        console.log("interstitialDidClose")
      );
      AdMobInterstitial.addEventListener("interstitialWillLeaveApplication", () =>
        console.log("interstitialWillLeaveApplication")
      );
  }
  componentWillUnmount() {
    console.log('will unmount search')
    AdMobInterstitial.removeAllListeners();
  }
  onPressMarker(e, index) {
    console.log('index', index)
    //console.log('stateOn press marker', this.state)
    const thing = this.state.things[index];
    this.setState({thing})
    if(thing.userId === this.props.helpers.user.id) {
      // const action = {
      //   type: "SELECT_OBJECT",
      //   value: {...thing}
      // };
      // this.props.dispatch(action);
      this.props.navigation.navigate("UpdateThingScreen", {sort: "UPDATE", from:"search", thing: {...thing}, onGoBack: () => this.getObjects() });
    } else {
      this.props.dispatch({
        type: "SPINNER",
        value: true
      });
      AdMobInterstitial.requestAdAsync().then((response)=>{
        console.log('response reward inter', response)
        if (!response) {
          AdMobInterstitial.showAdAsync();
          this.props.dispatch({
            type: "SPINNER",
            value: false
          });
        }
      }).catch((error)=>{
        console.log('error', error)
        AdMobInterstitial.showAdAsync();
        this.props.dispatch({
          type: "SPINNER",
          value: false
        }); 
      });
     
    }
    
  }

  render() {
      const dimensions = Dimensions.get('window');
      return (
          <View style={{
              flex: 1,
              flexDirection: "column",
              alignItems: "stretch"
            }}>
              <FilterModal
                visible={this.state.filterModalVisible} 
                close={this.closeFilter.bind(this)}
                selectCategory={this.selectCategory.bind(this)} 
                selectType={this.selectType.bind(this)}
                typeId={this.state.typeFilter}
                categoryId={this.state.categoryFilter}
                categories={this.state.categories}
                types={this.state.types}
                fromDate={this.state.fromDate}
                changeFromDate={this.changeFromDate}
                changeToDate={this.changeToDate}
                toDate={this.state.toDate}
                countResult={this.props.thingsState.things.length}
                hideCount={true}
                showDates={true}
              />
              <View style={styles.count}>
                <Text >
                  {i18n.t('searchScreen.found')} : {this.state.markers.length}
                </Text>
                <Button title={i18n.t('searchScreen.toList')}  onPress={() => this.props.navigation.navigate("ListScreen")}  />
              </View>

              <MapView
                  style={{ flex: 8, width: dimensions.width,  backgroundColor: "powderblue" }}
                  initialRegion={this.state.region}
                  onRegionChangeComplete={this.onRegionChange.bind(this)}
                  >
                  {this.state.markers.map((marker, index) => (
                      <MapView.Marker
                      key={index}
                      coordinate={marker.coordinates}
                      title={marker.title}
                      description={marker.description}
                      onPress={(e) => this.onPressMarker(e, index)}
                      >
                      <Image
                          source={marker.image}
                          style={{ width: 20, height: 20 }} 
                      />
                      </MapView.Marker>
                  ))}
                  <Text style={styles.count}>
                    {this.state.markers.length}
                  </Text>
              </MapView>
              <LinearGradient 
                colors={['#ff99c2', '#ff3385', '#FF0066']}
                start={{x: 0, y: 0}}
                end={{x: 1, y: 0}}
                style={styles.linearGradient}>
                <Text style={styles.buttonText}               
                  onPress={() => this.getObjects()}>
                  {i18n.t('searchScreen.searchZone')}
                </Text>
              </LinearGradient>
              {/* <Button title={i18n.t('searchScreen.searchZone')}  color="#FF0066" onPress={() => this.getObjects()}  /> */}
          </View>
      )
  }
}

const mapStateToProps = state => {
    //console.log("stateOverView", state);
    const { thingsState, helpers } = state;
    return { thingsState, helpers };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
      dispatch: action => {
        dispatch(action);
      }
    };
  };
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(SearchScreen);
  
  const styles = StyleSheet.create({
    takePicture: {
      position: "absolute",
      zIndex: 100,
      right: 5,
      bottom: 0
    },
    count: {
      color: "white", 
      position: 'absolute',
      left: 10,
      top: 10,
      right: 10,
      height: 50,
      zIndex: 1,
      backgroundColor: '#42A5F5',
    },
    input: {
      backgroundColor: '#42A5F5',
      margin: 10,
      padding: 8,
      color: 'white',
      borderRadius: 14,
      fontSize: 18,
      fontWeight: '500',
    },
    linearGradient: {
      flex:1,
      justifyContent: "center"
      // paddingLeft: 15,
      // paddingRight: 15,
      // borderRadius: 5,
      // marginTop:16,
      // width:350,
    },
    buttonText: {
      fontSize: 20,
      textAlign: 'center',
      color: '#ffffff',
      backgroundColor: 'transparent',
    }
  });