import React, { Component } from "react";
import { connect } from "react-redux";
import {
  ScrollView,
  Text,
  TextInput,
  View,
  Button,
  StyleSheet,
  AsyncStorage,
  Picker
} from "react-native";
import api from "../utils/api";
import DropDownHolder from "../utils/alert";
import i18n from '../utils/i18n';

class FormObjectScreen extends React.Component {
  static navigationOptions = {
    title: "Step1"
  };

  constructor(props) {
    super(props);
  }
  state = {
    categories: [],
    infos: {
      name: "Doudou",
      description: "peluche de lapin",
      date: "",
      time: ""
    }
  };
  async componentDidMount() {
    let self = this;
    console.log('form')
    const userToken = await AsyncStorage.getItem("userToken");
    console.log('form, token', userToken)
    try {
      api
        .get("/categories", {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + userToken
          }
        })
        .then(response => {
          console.log('response cat', JSON.stringify(response))
          self.setState({ categories: response.data }, () => {});
        })
        .catch(error => {
          console.log(error);
          const data =  error.response.data;
          DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message));
        });
    } catch (error) {
      console.error(error);
      const data =  error.response.data;
      DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message));
    }

    this.setState(
      {
        infos: this.props.currentThing.infos
      },
      function() {}
    );
  }
  saveToStore() {
    const action = {
      type: "ADD_DESCRIPTION",
      value: this.state.infos
    };
    this.props.dispatch(action);
  }
  render() {
    let serviceItems = this.state.categories.map((s, i) => {
      return <Picker.Item key={i} value={s.id} label={s.name} />;
    });
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Form Object</Text>
        <View style={styles.mainInfos}>
          <TextInput
            placeholder="Name"
            style={styles.infoInput}
            placeholder="Your name"
            value={this.props.currentThing.name}
            onChangeText={text => {
              return this.setState(
                prevState => ({
                  ...prevState,
                  infos: {
                    ...prevState.infos,
                    name: text
                  }
                }),
                () => {
                  this.saveToStore();
                }
              );
            }}
          />
          <TextInput
            placeholder="Description"
            style={styles.infoInput}
            value={this.props.currentThing.description}
            onChangeText={text => {
              return this.setState(
                prevState => ({
                  ...prevState,
                  infos: {
                    ...prevState.infos,
                    description: text
                  }
                }),
                () => {
                  this.saveToStore();
                }
              );
            }}
          />
          <TextInput placeholder="Contact" style={styles.infoInput} />
          {/* <Text>{JSON.stringify(this.props)}</Text>
          <Text>STATE</Text>
          <Text>{JSON.stringify(this.state)}</Text> */}
        </View>
        <Picker
          selectedValue={this.state.category}
          style={{ height: 50, width: 200 }}
          onValueChange={(itemValue, itemIndex) => {
            console.log("itemValue", itemValue);
            this.setState(
              prevState => ({
                ...prevState,
                infos: {
                  ...prevState.infos,
                  category: itemValue
                }
              }),
              () => {
                this.saveToStore();
              }
            );
          }}
        >
          {serviceItems}
        </Picker>
        <View style={{ margin: 7 }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 50
  },
  container: {
    flex: 1,
    alignItems: "center"
  },
  mainInfos: {
    flexDirection: "column"
  },
  infoInput: {
    width: 250,
    height: 50
  },

  dotStyle: {
    backgroundColor: "white",
    justifyContent: "space-between"
  },
  activeDotStyle: {
    backgroundColor: "orange"
  },
  wrapper: {},
  form: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "white"
  },
  map: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "white"
  },
  picture: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "white"
  },
  text: {
    color: "#fff",
    fontSize: 30,
    fontWeight: "bold"
  }
});

const mapStateToProps = state => {
  console.log("state", state);
  const { currentThing } = state;
  return { currentThing };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: action => {
      dispatch(action);
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FormObjectScreen);
