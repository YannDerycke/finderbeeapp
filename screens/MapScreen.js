import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Platform,
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  Button,
  Slider,
  Modal, 
  Dimensions,
  TouchableHighlight,
  
} from "react-native";
import {
  Constants,
  Location,
  Permissions,
  Marker,
  Circle
} from "expo";
import { LinearGradient } from 'expo-linear-gradient'
import i18n from '../utils/i18n'; 
import MapView from 'react-native-maps';
import { Ionicons } from "@expo/vector-icons";
import keys from "../assets/keys.png";


class MapScreen extends Component {
  static navigationOptions = {
    title: "MapScreen"
  };
  state = {
    location: null,
    errorMessage: null,
    markers: [],
    modalVisible: false,
    status:10
  };

  componentWillMount() {
    console.log('this currentThing map', this.props.currentThing)
    this.setState({
      region: {
        latitude: this.props.currentThing.geo.coordinates[1],
        longitude: this.props.currentThing.geo.coordinates[0],
        latitudeDelta: 0.01,
        longitudeDelta: 0.01
      }
    });

    this.addMarker(
      this.props.currentThing.geo.coordinates[1],
      this.props.currentThing.geo.coordinates[0]
    );
  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      this.setState({
        errorMessage: "Permission to access location was denied"
      });
    }

    let location = await Location.getCurrentPositionAsync({
      enableHighAccuracy: true
    });

    this.setState({
      region: {
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
        latitudeDelta: 0.01,
        longitudeDelta: 0.01
      }
    });
  };
  addMarker(lat, long) {
    //console.log('lat', lat)
    //console.log('long', long)

    this.setState({
      markers: [
        // ...this.state.markers,
        {
          image: keys,
          title: "lost",
          description: this.props.currentThing.description,
          coordinates: {
            latitude: lat,
            longitude: long
          }
        }
      ]
    });
    this.props.currentThing.geo = {
      coordinates : [long,lat]
    };
    
  }
  changeRadius = (radius) => {
    console.log('radius', radius)
    this.props.currentThing.radius = radius;
    this.setState(state => {
      state.radius = radius
      return state
    })
  }

  

  onRegionChange = infosLocation => {};
  toggleModal(visible) {
    console.log('vis', visible)
    console.log('nknknnknk', this.props.currentThing)
    this.setState({ modalVisible: visible, radius: this.props.currentThing.radius });
    if (visible) {
      if (this.props.currentThing) {
        //console.log("rpops Map", this.props);
        this.setState({
          region: {
            latitude: this.props.currentThing.geo.coordinates[1],
            longitude: this.props.currentThing.geo.coordinates[0],
            latitudeDelta: 0.01,
            longitudeDelta: 0.01
          }
        });
  
        this.addMarker(
          this.props.currentThing.geo.coordinates[1],
          this.props.currentThing.geo.coordinates[0]
        );
      } else {
        if (Platform.OS === "android" && !Constants.isDevice) {
          this.setState({
            errorMessage:
              "Oops, this will not work on Sketch in an Android emulator. Try it on your device!"
          });
          this.setState({
            region: {
              latitude: 50,
              longitude: 4,
              latitudeDelta: 0.01,
              longitudeDelta: 0.01
            }
          });
        } else {
          this._getLocationAsync();
        }
      }
    } else {
      this.props.sendInfos(this.props.currentThing)
    }
 }
  render() {
    const dimensions = Dimensions.get('window');
    // let text = "Waiting..";
    // if (this.state.errorMessage) {
    //   text = this.state.errorMessage;
    // } else if (this.state.location) {
    //   //      "latitude": 50.8264227,
    //   // "longitude": 4.3358164,
    //   this.state.region = {
    //     latitude: 50.8264227,
    //     longitude: 4.3358164,
    //     latitudeDelta: 0.0001,
    //     longitudeDelta: 0.0001
    //   };
    //   text = JSON.stringify(this.state.location);
    // }
    return (
      //   <View style={styles.container}>
      //     <Text style={styles.paragraph}>{text}</Text>
      //   </View>
      <View>
        <Modal animationType = {"slide"} transparent = {false}
          visible = {this.props.modalVisible}
          onRequestClose = {() => { console.log("Modal has been closed.") } }>

            <View
              style={{
                flex: 1,
                flexDirection: "column",
  
                alignItems: "stretch"
              }}
            >
              {/* <Text>{JSON.stringify(this.props.currentThing)}</Text> */}
              
              <MapView
                style={{ flex: 8, width: dimensions.width, backgroundColor: "powderblue" }}
                initialRegion={this.state.region}
                onPress={event => 
                  {
                    if (this.props.mode !== "VIEW") {
                      this.addMarker(
                        event.nativeEvent.coordinate.latitude,
                        event.nativeEvent.coordinate.longitude
                      )
                    }
                  }
                }
                onRegionChangeComplete={this.onRegionChange}
              >
                {this.state.markers.map((marker, index) => (
                  <MapView.Marker
                    key={index}
                    coordinate={marker.coordinates}
                    title={marker.title}
                    description={marker.description}
                    draggable
                  >
                    {/* <Image
                      source={marker.image}
                      style={{ width: 20, height: 20 }} 
                    /> */}
                    </MapView.Marker>
                ))}
                {this.state.markers.map((marker, index) => (
                  <MapView.Circle
                    key={index}
                    center={{
                      latitude: marker.coordinates.latitude,
                      longitude: marker.coordinates.longitude
                    }}
                    radius={this.props.currentThing.radius}
                    strokeWidth={1}
                    strokeColor={"#FF0066"}
                    fillColor={"rgba(255,0,102,0.1)"}
                  />
                ))}
              </MapView>
              {/* <Text>{this.props.currentThing.radius}</Text> */}
               {/* <Text>{JSON.stringify(this.props)}</Text> */}
              {this.props.showSlider ? <Slider 
                style={{
                  flex: 1,
                }}
                minimumTrackTintColor={"#FF0066"}
                thumbTintColor={"#FF0066"}
                value={this.props.currentThing.radius}
                onValueChange={value => this.changeRadius(value)}
                maximumValue={1000.0}
                minimumValue={10.0}
              />: null}
          </View>
          <View style={{ alignItems: "center", justifyContent: "center", flex: .1}}>
            {/* <Button
              style = {styles.button}
              title={i18n.t('mapScreen.close')}
              onPress={() => this.toggleModal(!this.props.modalVisible)}
            /> */}
            <View style={{flexDirection:"row", flex:1}}>
            <LinearGradient 
              colors={['#ff99c2', '#ff3385', '#FF0066']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={styles.linearGradient}>
              <Text style={styles.buttonText}               
                onPress={() => this.toggleModal(!this.props.modalVisible)}>
                {i18n.t('mapScreen.close')}
              </Text>
            </LinearGradient>
            </View>
          </View>
        </Modal> 
        {/* <Button
            title="Go to Map push"
            onPress={() => this.toggleModal(true)}
          /> */}
                    {/* <Ionicons
              style={styles.map}
              color={"#FF0066"}
              name="md-pin"
              // name={focused ? "ios-home" : "md-home"}
              onPress={() => this.toggleModal(true)}
              size={40}
            /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  map: {
    position: "absolute",
    left: 5,
    bottom: 0
  },
  markerFixed: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: "center",
    alignItems: "center"
  },
  marker: {
    height: 48,
    width: 48
  },
  footer: {
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    bottom: 0,
    position: "absolute",
    width: "100%"
  },
  region: {
    color: "#fff",
    lineHeight: 20,
    margin: 20
  },
  linearGradient: {
    flex:1,
    justifyContent: "center"
    // paddingLeft: 15,
    // paddingRight: 15,
    // borderRadius: 5,
    // marginTop:16,
    // width:350,
  },
  buttonText: {
    fontSize: 20,
    textAlignVertical: "center",
    textAlign: 'center',
    // margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
});

// const mapStateToProps = state => {
//   //console.log("stateMap", state);
//   const { currentThing } = state;
//   return { currentThing };
// };

// const mapDispatchToProps = dispatch => {
//   return {
//     dispatch: action => {
//       dispatch(action);
//     }
//   };
// };
// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(MapScreen);

export default MapScreen
