import React, { Component } from "react";
import { connect } from "react-redux";
import { Ionicons } from "@expo/vector-icons";
import { AdMobRewarded } from 'expo-ads-admob'
import {
  ScrollView,
  Text,
  TextInput,
  View,
  Button,
  StyleSheet,
  AsyncStorage,
  Picker,
  ListView,
  TouchableHighlight,
  Image,
  Dimensions,
  RefreshControl,
  Animated,
  
} from "react-native";
import moment from "moment";
import api from "../utils/api";
import DropDownHolder from "../utils/alert";
import i18n from '../utils/i18n';
import Header from "../components/Header"
import Images from '../assets/requireIcons';
import {getMyThings} from "../services/things";
import FilterModal from "./FilterModalScreen";

class ListScreen extends React.Component {

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params } = navigation.state;
    const dimensions = Dimensions.get('window');
    // return {
    //   header: <Header {...navigation} {...navigationOptions} />
    // };
    return {
      //header: null,
      // headerTitle: "Result Search",
      /* These values are used instead of the shared configuration! */
      // headerStyle: {
      //   backgroundColor: "green"
      // },
      // headerBackground: (
      //   <LinearGradient
      //     colors={['#579D9B', '#579D9B','#F8C25E','#F79F6D', '#F85479', '#F79F6D',  ]}
      //     style={{ flex: 1 }}
      //     start={{x: 0, y: 0}}
      //     end={{x: 1, y: 0}}
      //   />
      // ),
      headerRight: (
        <View>
           <View style={{flex:1, flexDirection:'row', justifyContent:'flex-end', alignContent:'flex-end'}}>
           <Ionicons
              color={"#FF0066"}
              name="ios-funnel"
              style={{marginRight:5}}
              onPress={() => params.showFilter()}
              size={40}
            />
          </View>
        </View>
      )

    };
  };
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      things: [],
      refreshing: false,
      categories: [],
      types: [],
      filterId: "",
      categoryId: "",
      filterModalVisible: false
    };
  }


  componentDidMount() {

    //this.getObjects();
    const { navigation } = this.props;

    const categories = [...this.state.categories, ...this.props.helpers.categories];
    this.setState({categories: [...this.state.categories, ...this.props.helpers.categories]})
    // console.log('categ', categories)
    const types = [...this.state.types, ...this.props.helpers.types];
    this.setState({types: [...this.state.types, ...this.props.helpers.types]})
    this.props.navigation.setParams({ 
      selectCategory: this.selectCategory.bind(this), 
      selectType: this.selectType.bind(this), 
      categories, 
      types, 
      typeId: this.props.thingsState.typeFilter,
      categoryId: this.props.thingsState.categoryFilter,
      showFilter: this.showFilter.bind(this)
    });
    // if (navigation.getParam('myThings')) {
    //   console.log('getobject')
    //   this.getObjects();
    // }
    AdMobRewarded.setAdUnitID("ca-app-pub-3940256099942544/5224354917");
    AdMobRewarded.removeAllListeners();

    AdMobRewarded.addEventListener("rewardedVideoDidLoad", () => {
      console.log("rewardedVideoDidLoad");
      this.props.dispatch({
        type: "SPINNER",
        value: false
      });
      AdMobRewarded.showAdAsync();
    });
    AdMobRewarded.addEventListener("rewardedVideoDidRewardUser", () => {
      this.props.dispatch({
        type: "SPINNER",
        value: false
      });
      console.log("rewardedVideoDidRewardUser")
      this.props.navigation.navigate("DetailsScreen", {thing: this.state.thing});
      }  
    );
    AdMobRewarded.addEventListener("rewardedVideoDidFailToLoad", () =>
      console.log("rewardedVideoDidFailToLoad")
    );
    AdMobRewarded.addEventListener("rewardedVideoDidOpen", () =>
      console.log("rewardedVideoDidOpen")
    );
    AdMobRewarded.addEventListener("rewardedVideoDidClose", () =>
      console.log("rewardedVideoDidClose")
    );
    AdMobRewarded.addEventListener("rewardedVideoWillLeaveApplication", () =>
      console.log("rewardedVideoWillLeaveApplication")
    );
  }
  componentWillUnmount() {
    console.log('will unmount list')
    AdMobRewarded.removeAllListeners();
  }

  selectCategory = (categoryId) => {
    //console.log('category', categoryId)
    //this.props.navigation.setParams({ categoryId: categoryId });
    this.setState({categoryFilter: categoryId})
    const action = {
      type: "FILTER_CATEGORY",
      value: categoryId
    };
    this.props.dispatch(action); 
    //console.log('d',this.props.thingsState.thingsFiltered.length)
    //this.setState({countResult: this.props.thingsState.thingsFiltered.length})
  };

  selectType = (typeId) => {
    //console.log('type', typeId)
    //this.props.navigation.setParams({ typeId: typeId });
    this.setState({typeFilter: typeId})
    const action = {
      type: "FILTER_TYPE",
      value: typeId
    };
    this.props.dispatch(action);
    //this.setState({countResult: this.props.thingsState.thingsFiltered.length})
  };

  showFilter() {
    this.setState({filterModalVisible: true})
  }
  closeFilter() {
    this.setState({filterModalVisible: false})
  }
  componentWillReceiveProps(nextProps) {
    // console.log('this.props.thingsState.typeFilter', this.props.thingsState.typeFilter)
    // console.log('nextProps typeFilter', nextProps.thingsState.typeFilter)
    // console.log('this.props.thingsState.categoryFilter', this.props.thingsState.categoryFilter)
    // console.log('nextProps categoryFilter', nextProps.thingsState.categoryFilter)
    // console.log('comparer', nextProps.thingsState.typeFilter !== this.props.thingsState.typeFilter)
    if (nextProps.thingsState.typeFilter !== this.props.thingsState.typeFilter) {
      //console.log('nextProps typeFilterIn', nextProps.thingsState.typeFilter)
      this.props.navigation.setParams({ typeId: nextProps.thingsState.typeFilter });
    }
    if (nextProps.thingsState.categoryFilter !== this.props.thingsState.categoryFilter) {
      //console.log('nextProps categoryFilterIn', nextProps.thingsState.categoryFilter)
      this.props.navigation.setParams({ categoryId: nextProps.thingsState.categoryFilter });
    }
  }

  async getObjects() {
    console.log('inGetObject list')
    let self = this;
    //console.log('things')
    const userToken = await AsyncStorage.getItem("userToken");
    //console.log('things, token', userToken)
    try {
      api
        .get("/things", {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + userToken
          }
        })
        .then(response => {
          //console.log("response.data", response.data[0]);
          const things = response.data;

          const action = {
            type: "LIST_THINGS",
            value:{
              things,
              categoryFilter: "",
              typeFilter: ""
            }
          };
          this.props.dispatch(action);
          //this.switch(this.state.filterId);
          self.setState(
            { loading:false,
              refreshing: false
            },
            function(e) {
              //console.log("callback set", e);
            }
          );
        })
        .catch(error => {
          console.log(error);
          const data =  error.response.data;
          DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message));
        });
    } catch (error) {
      console.error(error);
      const data =  error.response.data;
      DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message));
    }
  }

  goToDetails(row) {
    //console.log("row", row);
    console.log('row.list', row)
    this.setState({thing: row})
    if(row.userId === this.props.helpers.user.id) {
      // const action = {
      //   type: "SELECT_OBJECT",
      //   value: {...row}
      // };
      // this.props.dispatch(action);
      //this.props.navigation.navigate("AddScreen", {sort: "UPDATE", from:"myObject", thing: {...row}, onGoBack: () => this.getObjects(), });
      this.props.navigation.navigate("UpdateThingScreen", {sort: "UPDATE", from:"list", thing: {...row}, onGoBack: () => this.getObjects() });
    } else {
      this.props.dispatch({
        type: "SPINNER",
        value: true
      });
      AdMobRewarded.requestAdAsync().then((response)=>{
        console.log('response reward', response)
        if (!response) {
          AdMobRewarded.showAdAsync();
          this.props.dispatch({
            type: "SPINNER",
            value: false
          });
        }
      }).catch((error)=>{
        console.log('error', error)
        AdMobRewarded.showAdAsync();
        this.props.dispatch({
          type: "SPINNER",
          value: false
        }); 
      });
     
      
    }
  }

  refreshControl() {

  }
  render() {
    const { loading } = this.state;
    // if (loading) {
    //   return null;
    // }
    const dimensions = Dimensions.get('window');
    const imageHeight = Math.round(dimensions.width * 9 / 16);
    const imageWidth = dimensions.width - 20;
    
    return (

      <View style={{flex: 1}}>
        <FilterModal
          visible={this.state.filterModalVisible} 
          close={this.closeFilter.bind(this)}
          selectCategory={this.selectCategory.bind(this)} 
          selectType={this.selectType.bind(this)}
          typeId={this.props.thingsState.typeFilter}
          categoryId={this.props.thingsState.categoryFilter}
          categories={this.state.categories}
          types={this.state.types}
          countResult={this.props.thingsState.countFiltered}
        />
        <ScrollView 
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.refreshControl.bind(this)}
            />
          }
          scrollEventThrottle={16}
          onScroll = { Animated.event([
              {
                  nativeEvent: {
                      contentOffset: {
                          y: this.scrollY
                      }
                  }
              }
          ])}
        >
          <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap'}}>
            {this.props.thingsState.thingsFiltered.map(function (item, index) { 
              //console.log('item', item.name)
              //console.log('item', item)
              //  const {type, Poster, Title} = item;
              let type = item.type ? item.type.name : "noName";
              let category = item.category ? item.category.name : "noName";
              return (
              //key is the index of the array 
              //item is the single item of the array
              <View key={index}>
                <TouchableHighlight  onPress={() => this.goToDetails(item)}>
                  <View>
                    <Image
                      style={{ width:  (dimensions.width/2)-10, height: 200, margin:5 }}
                      source={{
                        uri:
                        item.picture === "azerty"
                            ? "https://via.placeholder.com/70x70.jpg"
                            : item.picture
                      }}
                    />
                    <Image
                      style={{ width:  40,  height: 40, position: "absolute", left: 5, top: 5 }}
                      source={Images[item.category.image]}
                    />
                    {/* <Text>{item.name}</Text> */}
                    {/* <Text>{item.picture}</Text> */}
                    {/* <Text>{item.geo.coordinates[0]}</Text>
                    <Text>{item.geo.coordinates[1]}</Text>
                    <Text>{type}</Text>
                    <Text>{category}</Text> */}
                    {/* <Text>{item.category.name}</Text> */}
                    {/* <Text style={styles.text}>{item.description}</Text> */}
                  </View>
                </TouchableHighlight>
              </View>
            )}.bind(this))}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  item: {

  },
  listItem: {
    flex: 1,
    flexDirection: "row",
    borderBottomWidth: 1,
    borderBottomColor: "#d6d7da",
    padding: 6
  },
  icon: {
    position: "absolute",

  },
  
  imageWrapper: {
    padding: 5
  },
  title: {
    fontSize: 20,
    textAlign: "left",
    margin: 6
  },
  subtitle: {
    fontSize: 10,
    textAlign: "left",
    margin: 6
  }
});

const mapStateToProps = state => {
  //console.log("state", state);
  const { thingsState, helpers } = state;
  return { thingsState, helpers};
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: action => {
      dispatch(action);
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListScreen);
