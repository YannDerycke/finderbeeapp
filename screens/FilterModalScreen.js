import React, {Component} from 'react';
import {Modal, Text, TouchableHighlight, View, Alert, Picker, StyleSheet} from 'react-native';
import { Linking, LinearGradient} from "expo";
import DatePicker from 'react-native-datepicker';
import i18n from '../utils/i18n';

class FilterModal extends Component {
  state = {
    filterModalVisible: false,
  };

  setModalVisible(visible) {
    this.setState({filterModalVisible: visible});
  }

  render() {
    return (
      <View style={{marginTop: 22}}>
        <Modal
          animationType="slide"
          transparent={false}
        //   visible={this.state.filterModalVisible}
          visible={this.props.visible}
          onRequestClose={() => {
            // Alert.alert('Modal has been closed.');
            this.props.close(false)
          }}>
          <View style={{flex:.3, marginTop: 22}}>
            <View style={{flex:1, flexDirection:"row"}}>
              <View  style={{flex:1}}>
                <Picker
                    selectedValue={this.props.categoryId}
                    // style={{ width: dimensions.width/4 }}
                    onValueChange={this.props.selectCategory}>
                    {this.props.categories.map(category => {
                        return <Picker.Item key={category.id} label={category.name} value={category.id} />
                    })}
                </Picker>
              </View>

              <View  style={{flex:1}}>
                <Picker
                    selectedValue={this.props.typeId}
                  //   style={{  width: dimensions.width/4 }}
                    onValueChange={this.props.selectType}>
                    {this.props.types.map(type => {
                        return <Picker.Item key={type.id} label={type.name} value={type.id} />
                    })}
                </Picker>        
              </View>      
            </View>
            {this.props.showDates ?         
            <View style={{flex:1, flexDirection:"row"}}>
              <View  style={{flex:1, flexDirection:"row"}}> 
                  <DatePicker
                    style={{flex:2}}
                    date={this.props.fromDate} //initial date from state
                    mode="date" //The enum of date, datetime and time
                    placeholder="select date"
                    format="DD-MM-YYYY"
                    minDate="01-01-2016"
                    maxDate="01-01-2020"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        left: 0,
                        top: 4,
                        marginLeft: 0
                      },
                      dateInput: {
                        marginLeft: 36
                      }
                    }}
                    onDateChange={(date) => {this.props.changeFromDate(date);}}
                  />
                <Text  style={{flex:1, textAlign:"center"}} onPress={()=>{
                    this.props.changeFromDate(null)
                }}>{i18n.t('filterScreen.reset')}</Text>

              </View>
              <View  style={{flex:1, flexDirection:"row"}}>
                <View  style={{flex:1, flexDirection:"row"}}>         
                  <DatePicker
                    style={{flex:2}}
                    date={this.props.toDate} //initial date from state
                    mode="date" //The enum of date, datetime and time
                    placeholder="select date"
                    format="DD-MM-YYYY"
                    minDate="01-01-1900"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        left: 0,
                        top: 4,
                        marginLeft: 0
                      },
                      dateInput: {
                        marginLeft: 36
                      }
                    }}
                    onDateChange={(date) => {this.props.changeToDate(date);}}
                  />

                  <Text style={{flex:1, textAlign:"center"}} onPress={()=>{
                    this.props.changeToDate(null)
                  }}>{i18n.t('filterScreen.reset')}</Text>
                </View>  
              </View>
            </View>
            : null }
            <View style={{flex:.5, flexDirection:"row"}}>
                <LinearGradient 
                    colors={['#ff99c2', '#ff3385', '#FF0066']}
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 0}}
                    style={{flex:1}}>
                    {this.props.hideCount ? 
                      <Text style={styles.buttonText}               
                          onPress={() =>  this.props.close(false)}>
                          {i18n.t('filterScreen.search')}   
                      </Text>:
                      <Text style={styles.buttonText}               
                          onPress={() =>  this.props.close(false)}>
                          {i18n.t('filterScreen.result')} {this.props.countResult } {i18n.t('filterScreen.things')}
                      </Text>
                    }
                </LinearGradient>
              </View>
              {/* <LinearGradient 
                  colors={['#9cecfb', '#65C7F7', '#0052D4']}
                  start={{x: 0, y: 0}}
                  end={{x: 1, y: 0}}
                  style={styles.linearGradient}>
                      {this.props.hideCount ? 
                  <Text style={styles.buttonText}               
                      onPress={() =>  this.props.close(false)}>
                      {i18n.t('filterScreen.search')}   
                  </Text>:
                  <Text style={styles.buttonText}               
                      onPress={() =>  this.props.close(false)}>
                      {i18n.t('filterScreen.result')} {this.props.countResult } {i18n.t('filterScreen.things')}
                  </Text>
                  }
              </LinearGradient> */}
          </View>
        </Modal>
{/* 
        <TouchableHighlight
          onPress={() => {
            this.setModalVisible(true);
          }}>
          <Text>Show Modal</Text>
        </TouchableHighlight> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    linearGradient: {
      paddingLeft: 15,
      paddingRight: 15,
      borderRadius: 5,
      marginTop:16,
      width:350,
    },
    buttonText: {
      fontSize: 18,
      textAlign: 'center',
      margin: 10,
      color: '#ffffff',
      backgroundColor: 'transparent',
    },
  });
export default FilterModal