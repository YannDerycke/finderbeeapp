import React from "react";
import { Text, View, Button, Image, AsyncStorage, } from "react-native";

import { StackNavigator } from "react-navigation";
import { Permissions, Notifications } from "expo";
import api from "../utils/api";

const PUSH_ENDPOINT = "https://your-server.com/users/push-token";

class PushNotificationScreen extends React.Component {
  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params } = navigation.state;

    return {
      // title: "Push",
      // headerRight: (
      //   <Button
      //     onPress={navigation.getParam("openDraw")}
      //     title="Menu"
      //     color="pink"
      //   />
      // ),
      // headerTintColor: "yellow"
    };
  };
  constructor(props) {
    super(props);
    this.state = { userOne: "" };

    // this.props.navigation.navigate("AddMobScreen", { idFilm: "idFilm" });
  }

  async componentDidMount() {
    this.props.navigation.setParams({
      openDraw: this._openDraw
    });
  }
  _openDraw = () => {
    console.log("fds");
    this.props.navigation.openDrawer();
  };

  async registerForPushNotificationsAsync() {
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );
    console.log("exist", existingStatus);
    let finalStatus = existingStatus;

    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== "granted") {
      // Android remote notification permissions are granted during the app
      // install, so this will only ask on iOS
      const { status } = await Permissions.askAsync(
        Permissions.NOTIFICATIONS
      );
      finalStatus = status;
    }

    // Stop here if the user did not grant permissions
    if (finalStatus !== "granted") {
      return;
    }

    // Get the token that uniquely identifies this device
    let token = await Notifications.getExpoPushTokenAsync();
    console.log('token', token)
    this.setState({
      token
    })
    // POST the token to your backend server from where you can retrieve it to send push notifications.
    // return fetch("https://finder-bee-server.herokuapp.com/oauth/facebook", {
    //   method: "POST",
    //   headers: {
    //     Accept: "application/json",
    //     "Content-Type": "application/json"
    //   },
    //   body: JSON.stringify({
    //     pushnotification: token
    //   })
    // });
    const userToken = await AsyncStorage.getItem("userToken");
    api.post("/users/pushnotification", JSON.stringify({
      pushnotification: token
    }), {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
        Authorization: "Bearer " + userToken
      }
    })
    .then(async response => {
      //console.log('response', JSON.stringify(response))
      const data =  response.data;
      console.log('data', data)
      // console.log('data', data)
      //this.dropDownAlertRef.alertWithType('success', i18n.t('message'), i18n.t(data.message));
      // DropDownHolder.getDropDown().alertWithType('success', i18n.t('message'), i18n.t(data.message));
      // this.props.navigation.navigate("App");
    })
    .catch(error => {
      const data =  error.response.data;
      console.log('data', data)
      // this.dropDownAlertRef.alertWithType('error', i18n.t('message'), i18n.t(data.message));
      //DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message));
    });
  }

  render() {
    
    return (
      <View style={{ alignItems: "center", justifyContent: "center", flex: 1 }}>
        <Text onPress={this._handlePress}>PushNotificationScreenMF!</Text>
        <Button
          title="pushSendToken"
          onPress={() => this.registerForPushNotificationsAsync()}
        />
        <Text> {this.state.users}</Text>
        <Text> {this.state.token}</Text>
      </View>
    );
  }

  _handlePress = () => {
    this.props.navigation.navigate("Home");
  };
}

export default PushNotificationScreen;
