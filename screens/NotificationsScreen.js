import React from "react";
import { connect } from "react-redux";
import { 
  Text, 
  View, 
  Button, 
  Image, 
  AsyncStorage, 
  StatusBar, 
  StyleSheet, 
  ScrollView, 
  Dimensions,
  TouchableHighlight,
  RefreshControl,
  Animated
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Images from '../assets/requireIcons';

import { StackNavigator } from "react-navigation";
import { Permissions, Notifications, LinearGradient } from "expo";
import api from "../utils/api";
// import {reward, interstitial} from "../utils/admob"
import { AdMobRewarded } from 'expo-ads-admob'

const PUSH_ENDPOINT = "https://your-server.com/users/push-token";

class NotificationScreen extends React.Component {
  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params } = navigation.state;

    return {
      // title: "Push",
      // headerRight: (
      //   <Button
      //     onPress={navigation.getParam("openDraw")}
      //     title="Menu"
      //     color="pink"
      //   />
      // ),
      // headerTintColor: "yellow"
    };
  };
  constructor(props) {
    super(props);
    this.state = { notifications: [] };

    // this.props.navigation.navigate("AddMobScreen", { idFilm: "idFilm" });
  }

  async componentDidMount() {
    this.props.dispatch({
      type: "SPINNER",
      value: true
    });
    this.props.navigation.setParams({
      openDraw: this._openDraw
    });
    const userToken = await AsyncStorage.getItem("userToken");
    try {
      api
        .get("/notifications/mine", {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + userToken
          }
        })
        .then(response => {
          this.props.dispatch({
            type: "SPINNER",
            value: false
          });
          console.log("response.notifications type", response.data);
          const notifications = response.data;
          this.setState({notifications})
        })
    } catch (error) {
      this.props.dispatch({
        type: "SPINNER",
        value: false
      });
      console.error(error);
      const data =  error.response.data;
      //DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message));
    }
    AdMobRewarded.setAdUnitID("ca-app-pub-3940256099942544/5224354917");
    AdMobRewarded.removeAllListeners();

    AdMobRewarded.addEventListener("rewardedVideoDidLoad", () => {
      console.log("rewardedVideoDidLoad");
      this.props.dispatch({
        type: "SPINNER",
        value: false
      });
      AdMobRewarded.showAdAsync();
    });


    AdMobRewarded.addEventListener("rewardedVideoDidRewardUser", () => {
      this.props.dispatch({
        type: "SPINNER",
        value: false
      });
      console.log("rewardedVideoDidRewardUser")
        this.props.navigation.navigate("DetailsScreen", {thing: this.state.thing})
      }  
    );
    AdMobRewarded.addEventListener("rewardedVideoDidFailToLoad", () =>
      {  console.log("rewardedVideoDidFailToLoad")
        this.props.navigation.navigate("DetailsScreen", {thing: this.state.thing})
      }
    );
    AdMobRewarded.addEventListener("rewardedVideoDidOpen", () =>
      console.log("rewardedVideoDidOpen")
    );
    AdMobRewarded.addEventListener("rewardedVideoDidClose", () =>
      console.log("rewardedVideoDidClose")
    );
    AdMobRewarded.addEventListener("rewardedVideoWillLeaveApplication", () =>
      console.log("rewardedVideoWillLeaveApplication")
    );
  }
  componentWillUnmount() {
    console.log('will unmount notif')
    AdMobRewarded.removeAllListeners();
  }

  async refresh() {
    const userToken = await AsyncStorage.getItem("userToken");
    try {
      api
        .get("/notifications/mine", {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + userToken
          }
        })
        .then(response => {
          console.log("response.notifications type", response.data);
          const notifications = response.data;
          this.setState({notifications, refreshing:false})
        })
        .catch(error => {
          console.log(error);
          const data =  error.response.data;
          //DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message));
        });
    } catch (error) {
      console.error(error);
      const data =  error.response.data;
      //DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message));
    }

  }
  async erase(notification) {
    const userToken = await AsyncStorage.getItem("userToken");
    try {
      api
      .delete(`/notifications/${notification.id}`, {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + userToken
          }
        })
        .then(response => {
          console.log("response.notifications delete", response.data);
          const notifications = this.state.notifications.filter((notif)=>{
            return notif.id !== notification.id
          })
          this.setState({notifications})
        })
        .catch(error => {
          console.log(error);
          const data =  error.response.data;
          //DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message));
        });
    } catch (error) {
      console.error(error);
      const data =  error.response.data;
      //DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message));
    }
  }
  goDetails(notif) {
    console.log('notif', notif.thing)
    this.props.dispatch({
      type: "SPINNER",
      value: true
    });
    //console.log('notuf', notif)
    this.setState({thing: notif.thing})
   
    AdMobRewarded.requestAdAsync().then((response)=>{
      console.log('response reward', response)
      if (!response) {
        // AdMobRewarded.showAdAsync();
        this.props.dispatch({
          type: "SPINNER",
          value: false
        });
        //this.props.navigation.navigate("DetailsScreen", {thing: notif.thing})
      }
    }).catch((error)=>{
      console.log('error', error)
      AdMobRewarded.showAdAsync();
      this.props.dispatch({
        type: "SPINNER",
        value: false
      });
      this.props.navigation.navigate("DetailsScreen", {thing: notif.thing})
    });
    
    
  }

  render() {
    const dimensions = Dimensions.get('window');
    const imageHeight = Math.round(dimensions.width * 9 / 16);
    const imageWidth = dimensions.width - 20;
    return (
      <ScrollView 
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.refresh.bind(this)}
            />
          }
          scrollEventThrottle={16}
          onScroll = { Animated.event([
              {
                  nativeEvent: {
                      contentOffset: {
                          y: this.scrollY
                      }
                  }
              }
          ])}
        >
          <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap'}}>
          {this.state.notifications.map(function (notif, index) { 
 
            return (
            //key is the index of the array 
            //item is the single item of the array
            <View key={index}>
               {/* <Text>{notif.thing.name}</Text> */}
              <TouchableHighlight  onPress={() => this.goDetails(notif)}>
                <View style={{ width:  (dimensions.width/2)-10, height: 200, margin:5 }}>
                  <Image
                    style={{ width:  (dimensions.width/2)-10, height: 200, margin:5 }}
                    source={{
                      uri:
                      notif.thing.picture === "azerty"
                          ? "https://via.placeholder.com/70x70.jpg"
                          : notif.thing.picture
                    }}
                  />
                  <Image
                    style={{ width:  40,  height: 40, position: "absolute", left: 5, top: 5 }}
                    source={Images[notif.thing.category.name]}
                  />
                  <Ionicons
                      style={{ position: "absolute", right: 5, top: 5 }}
                      name="md-trash"
                      color= "#FF0066"
                      onPress={() => this.erase(notif)}
                      size={40} 
                      
                    />
                
                  {/* <Text>{item.user.surname}</Text>
                  <Text>{item.user.name}}</Text> */}
                  <Text>{notif.thing.name}</Text>
                  {/* <Text>{item.picture}</Text> */}
                  {/* <Text>{item.geo.coordinates[0]}</Text>
                  <Text>{item.geo.coordinates[1]}</Text>
                  <Text>{type}</Text>
                  <Text>{category}</Text> */}
                  {/* <Text>{item.category.name}</Text> */}
                  {/* <Text style={styles.text}>{item.description}</Text> */}
                </View>
              </TouchableHighlight>
            </View>
          )}.bind(this))}

        </View>
      </ScrollView>
    );
  }

  _handlePress = () => {
    this.props.navigation.navigate("Home");
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  linearGradient: {
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5,
    marginTop:16,
    width:350,
  },
  buttonText: {
    fontSize: 18,
   
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
});


const mapStateToProps = state => {
  //console.log("stateOverView", state);
  const { helpers } = state;
  return { helpers };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: action => {
      dispatch(action);
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NotificationScreen);

