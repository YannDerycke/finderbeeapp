import React from "react";
import { Ionicons } from "@expo/vector-icons";
import { connect } from "react-redux";
import { Text, View, Button, Image, Icon, AsyncStorage, StyleSheet, Picker } from "react-native";
import { Linking, LinearGradient, Notifications, AdMobBanner} from "expo";
import api from "../utils/api";
import i18n from '../utils/i18n';

import { StackNavigator } from "react-navigation";

class HomeScreen extends React.Component {
  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params } = navigation.state;

    return {
      title: "A Nested Details Screen",
      /* These values are used instead of the shared configuration! */
      headerStyle: {
        backgroundColor: "white",
        color:"black"
      },
      // headerBackground: (
      //   <LinearGradient
      //     colors={['#711c91', '#ea00d9', '#0abdc6', '#133e7c', '#091833']}
      //     style={{ flex: 1 }}
      //     start={{x: 0, y: 0}}
      //     end={{x: 1, y: 0}}
      //   />
      // )
    };
  };

  constructor(props) {
    super(props);
    console.log('ihihi')
    
    //console.log("this.props.", this.props);
    // this.props.navigation.openDrawer();
    //this.props.navigation.navigate("AddMobScreen", { idFilm: "idFilm" });
  }
  state = { urilink: "", count: 1, language: "en" };
  async componentDidMount() {
    const userToken = await AsyncStorage.getItem("userToken");
    console.log('userToken',userToken)
    if (!userToken) {
      this.props.navigation.navigate("Auth");
    }
    this.setState({userToken});
    const language = await AsyncStorage.getItem("language");
    console.log('language', language) 
    if( language) {
      this.setState({language})
      i18n.locale = language; 
    }
    
    var subscription = Notifications.addListener(this.handleNotification.bind(this));

    const uriPrefix = Linking.makeUrl();
    console.log("utiPrefix", uriPrefix);
    this.setState({ urilink: uriPrefix }); 
    api
    .get("/users/me", {  
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + userToken
      }
    }).then(async response => {
      console.log('response', JSON.stringify(response))
      const data =  response.data;
      const action = {
        type: "USER",
        value: data
      };
      this.props.dispatch(action);
      console.log('dataME', data)

    })
    .catch(error => {
      console.log('error', error)

    });
    this.props.dispatch({
      type: "USER",
      value:{
        id:86,
        name:"Jean",
        surname:"Martin" 
      }
    });
    this.getTranslationTypeCat();
  }

  async getTranslationTypeCat() {
    this.props.dispatch({
      type: "SPINNER",
      value: true
    });
    const userToken = await AsyncStorage.getItem("userToken");
    console.log('things, token', userToken)
    const promiseTypes = api
    .get("/types", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + userToken
      }
    })
    const promiseCategories  = api
    .get("/categories", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + userToken
      }
    })
    Promise.all([promiseTypes, promiseCategories]).then((arrayValues) => {
      this.props.dispatch({
        type: "SPINNER",
        value: false
      });
      // console.log('promises', JSON.stringify(arrayValues));
      const types = arrayValues[0].data;
      types.map((type)=>{
        type.originName = type.name;
        type.name =   i18n.t('types.' + type.name)
        return type
      })
      types.unshift({id:'',name:i18n.t('types.ALL')})
      console.log('types', types)
      this.props.dispatch({
        type: "ADD_TYPES",
        value: types
      });
      const categories = arrayValues[1].data;
      categories.map((category)=>{
        category.originName = category.name;
        category.name = i18n.t('categories.' + category.name)
        return category
      })
      categories.unshift({id:'',name:i18n.t('categories.ALL')})
      console.log('categories', categories)      
      this.props.dispatch({
        type: "ADD_CATEGORIES",
        value: categories
      }); 

    }).catch(error => {
      console.error('error', error);
      this.props.dispatch({
        type: "SPINNER",
        value: false
      });
      const data =  error;
      if (error.response) {
        data =  error.response.data;
      }
      DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message));
    });
  }

  handleNotification(notif) {
    console.log('notif', notif)
    this.props.navigation.navigate(notif.data.screen, { id: notif.data.id })
  }
  async logOut() {
    await AsyncStorage.removeItem("userToken"); 
    this.props.navigation.navigate("AuthLoading");
    // const action = {
    //   type: "EMPTY_MY_THINGS",
    //   value: null
    // };
    // this.props.dispatch(action);
  }

  async changeLanguage(language) {
   
    console.log('lang', language)
    i18n.locale = language;
    // this.setState({language: language})
    this.setState({ language });
    this.getTranslationTypeCat();
    await AsyncStorage.setItem("language", language);
    
  }
  render() {
    return (
      <View style={{ alignItems: "center", justifyContent: "center", flex: 1 }}>
        <Text onPress={this._handlePress}>HomeScreenMF!</Text>
        <Text>user token: {this.state.userToken}</Text>
        <Text>urilink: {this.state.urilink}</Text>
        <LinearGradient 
          colors={['#ff99c2', '#ff3385', '#FF0066']}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          style={styles.linearGradient}>
          <Text style={styles.buttonText}               
            onPress={() => this.props.navigation.navigate("MyObjectsScreen", {myThings: true, force: false })}>
            Go to My objects
          </Text>
        </LinearGradient>
        {/* <LinearGradient 
          colors={['#ff99c2', '#ff3385', '#FF0066']}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          style={styles.linearGradient}>
          <Text style={styles.buttonText}               
            onPress={() => this.props.navigation.navigate("UserScreen", {myThings: true, force: false })}>
            My infos
          </Text>
        </LinearGradient> */}
        {/* <Button color="#FF0066" title={i18n.t('logOut')} onPress={() =>{ this.logOut() }} /> */}

        {/* <View style={{ flex:.4, flexDirection:"row",  alignItems: "center", justifyContent: "center" }}>
          <Picker style={{flex:1}}  selectedValue = {this.state.language}  onValueChange = {(value) => {this.changeLanguage(value)}}>
            <Picker.Item label="English" value="en"/>
            <Picker.Item label="Français" value="fr"/>
            <Picker.Item label="Español" value="es"/>
          </Picker>    
        </View> TO DO manage locale header title issue */} 
        
        {/* <Text> {this.state.language}</Text> */}
        <AdMobBanner
          style={styles.bottomBanner}
          bannerSize="fullBanner"
          adUnitID="ca-app-pub-3940256099942544/6300978111" // Test ID, Replace with your-admob-unit-id
          didFailToReceiveAdWithError={this.bannerError}
        />
      </View>
    );
  }

  _handlePress = () => {
    this.props.navigation.navigate("Home");
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  linearGradient: {
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5,
    marginTop:16,
    width:350,
  },
  buttonText: {
    fontSize: 18,
   
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  bottomBanner: {
    position: "absolute",
    bottom: 0
  },
});

const mapStateToProps = state => {
  //console.log("stateOverView", state);
  const { helpers } = state;
  return { helpers };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: action => {
      dispatch(action);
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen);

