import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { AdMobBanner, AdMobInterstitial, AdMobRewarded } from "expo";

export default class AddMob extends React.Component {
  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params } = navigation.state;

    return {
      title: "AddMob",
      /* These values are used instead of the shared configuration! */
      headerTintColor: "blue"
    };
  };
  componentDidMount() {
    this.props.navigation.setParams({
      openDraw: this._openDraw
    });

    const deviceId = Expo.Constants.deviceId;
    console.log("deviceId", deviceId);
    // AdMobInterstitial.setTestDeviceID(deviceId);
    AdMobInterstitial.setTestDeviceID("EMULATOR");
    // ALWAYS USE TEST ID for Admob ads
    AdMobInterstitial.setAdUnitID("ca-app-pub-3940256099942544/1033173712");

    AdMobInterstitial.addEventListener("interstitialDidLoad", () => {
      console.log("interstitialDidLoad");
      AdMobInterstitial.showAdAsync();
    });

    AdMobInterstitial.addEventListener("interstitialDidFailToLoad", () =>
      console.log("interstitialDidFailToLoad")
    );

    AdMobInterstitial.addEventListener("interstitialDidOpen", () =>
      console.log("interstitialDidOpen")
    );
    AdMobInterstitial.addEventListener("interstitialDidClose", () =>
      console.log("interstitialDidClose")
    );
    AdMobInterstitial.addEventListener("interstitialWillLeaveApplication", () =>
      console.log("interstitialWillLeaveApplication")
    );
    // AdMobRewarded.setTestDeviceID(deviceId);
    AdMobInterstitial.setTestDeviceID("EMULATOR");
    // ALWAYS USE TEST ID for Admob ads
    AdMobRewarded.setAdUnitID("ca-app-pub-3940256099942544/5224354917");

    AdMobRewarded.addEventListener("rewardedVideoDidRewardUser", () =>
      console.log("inAdMobRewardedDidLoad")
    );
    AdMobRewarded.addEventListener("rewardedVideoDidLoad", () => {
      console.log("interstitialDidLoad");
      AdMobRewarded.showAdAsync();
    });
    AdMobRewarded.addEventListener("rewardedVideoDidFailToLoad", () =>
      console.log("interstitialDidLoad")
    );
    AdMobRewarded.addEventListener("rewardedVideoDidOpen", () =>
      console.log("interstitialDidLoad")
    );
    AdMobRewarded.addEventListener("rewardedVideoDidClose", () =>
      console.log("interstitialDidLoad")
    );
    AdMobRewarded.addEventListener("rewardedVideoWillLeaveApplication", () =>
      console.log("interstitialDidLoad")
    );
  }

  componentWillUnmount() {
    AdMobInterstitial.removeAllListeners();
  }
  _openDraw = () => {
    console.log("fds");
    this.props.navigation.openDrawer();
  };
  bannerError() {
    console.log("An error");
    return;
  }

  showInterstitial() {
    console.log("AdMobInterstitial", AdMobInterstitial);
    AdMobInterstitial.requestAdAsync();
  }

  showRewarded() {
    // first - load ads and only then - show
    console.log("AdMobRewarded", AdMobRewarded);
    AdMobRewarded.requestAdAsync();
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
        <Button
          title="Interstitial"
          onPress={this.showInterstitial}
          containerViewStyle={styles.interstitialBanner}
        />
        <Button
          title="Rewarded"
          onPress={this.showRewarded}
          containerViewStyle={styles.rewardedBanner}
        />

        {/* <Text>Test id test AddMob!</Text>
        <AdMobBanner
          style={styles.bottomBanner}
          bannerSize="fullBanner"
          adUnitID="ca-app-pub-3940256099942544/6300978111" // Test ID, Replace with your-admob-unit-id fake
          didFailToReceiveAdWithError={this.bannerError}
        /> */}
        <AdMobBanner
          style={styles.bottomBanner}
          bannerSize="fullBanner"
          adUnitID="ca-app-pub-3940256099942544/6300978111" // Test ID, Replace with your-admob-unit-id
          didFailToReceiveAdWithError={this.bannerError}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  interstitialBanner: {
    width: "100%",
    marginLeft: 0
  },
  bottomBanner: {
    position: "absolute",
    bottom: 0
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
