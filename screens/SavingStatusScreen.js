import React, { Component } from "react";
import { Ionicons } from "@expo/vector-icons";
import { connect } from "react-redux";
import {
  ScrollView,
  Text,
  TextInput,
  View,
  Button,
  StyleSheet,
  AsyncStorage,
  Picker,
  Header
} from "react-native";
import loginFB from "../utils/auth";
import TakePicture from "../screens/TakePictureScreen";
import FormScreen from "../screens/FormScreen";
import MapScreen from "../screens/MapScreen";

class SavingStatusScreen extends React.Component {
  constructor(props) {
    super(props);
    //console.log("this.props", this.props);
  }
  state = {
    selectedCategory: "js"
  };

  static navigationOptions = {
    title: "Step1",
    drawerIcon: ({ tintColor }) => (
      <Ionicons name="md-add" size={32} color="orange" />
    )
  };
  componentDidMount() {}
  getStyle = function(property) {
    let style = { width: 50, height: 50, backgroundColor: "green" };
    console.log(
      "this.props.findObject[property].length ",
      this.props.findObject[property]
    );
    if (
      Object.keys(this.props.findObject[property]).length === 0 &&
      this.props.findObject[property].constructor === Object
    ) {
      style.backgroundColor = "red";
    }
    return style;
  };
  render() {
    return (
      //   <View style={{ flex: 1 }}>
      //     <Text >Saving</Text>
      //     <Text>{JSON.stringify(this.props.findObject)}</Text>
      //   </View>
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "stretch"
        }}
      >
        <View style={this.getStyle("category")} />
        <View style={this.getStyle("location")} />
        <View style={this.getStyle("picture")} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },

  dotStyle: {
    backgroundColor: "white"
  },
  activeDotStyle: {
    backgroundColor: "orange"
  },
  wrapper: {},
  slide1: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#5c77a9"
  },
  slide2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#5ca1a9"
  },
  slide3: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#69a95c"
  },
  text: {
    color: "#fff",
    fontSize: 30,
    fontWeight: "bold"
  }
});

const mapStateToProps = state => {
  return {
    findObject: state.findObject
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: action => {
      dispatch(action);
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SavingStatusScreen);
