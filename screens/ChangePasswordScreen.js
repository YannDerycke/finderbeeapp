import React, { Component } from "react";
import { connect } from "react-redux";
import {
  ScrollView,
  Text,
  TextInput, 
  View,
  Button,
  StyleSheet,
  AsyncStorage
} from "react-native";
import TextField from '../components/TextField'
import {validate, validateAll, comparePassword} from "../utils/validate";
import DropDownHolder from "../utils/alert";
import i18n from '../utils/i18n';
import api from "../utils/api";
// import App from "../App"


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  input: {
    backgroundColor: '#42A5F5',
    margin: 10,
    padding: 8,
    color: 'white',
    borderRadius: 14,
    fontSize: 18,
    fontWeight: '500',
  }
});
class ChangePasswordScreen extends React.Component {
  // static navigationOptions = ({ navigation, navigationOptions }) => {
  //   const { params = {} } = navigation.state;
  //   return {
  //     headerTitle: params.title,
  //   };
  // };
  constructor(props) {
    super(props)
    this.state = {user:  {}, errorForm: {name:"", surname:"", email:"", password:"", language: i18n.locale}}
    // this.state = {user:this.props.helpers.user, errorForm: {name:"", surname:"", email:"", password:""}}
  }
  componentDidMount() {  

    console.log('this.props', this.props.helpers)
    // const { navigation } = this.props;
    // console.log('nav', navigation)
    // navigation.setParams({ title: i18n.t('signUp') });
  }
  goBack = () => {
    this.props.navigation.navigate("HomeScreen");
  }
  onChangeText = (property, value) => { 
    
    this.setState({ user: { ...this.state.user, [property]: value} });
    console.log('thisstate', this.state)
  }
  validateInput = (property) => {
    // let errorValue;
    // errorValue = validate(property, this.state.user[property]);
    // this.setState({
    //   errorForm: {...this.state.errorForm, [property]: errorValue}
    // })
  }
  modify = async () => {
    const user = this.state.user; 
    user.pushnotification = this.props.helpers.pushToken
    console.log('user', user)
    const testForm = validateAll(user, 'modifyPassword');
    console.log('testForm', testForm)
    this.setState({
      errorForm: testForm === null ? {} : testForm
    })
    let validForm = !testForm;
    // console.log('user', user)
    if(validForm) {
      const userToken = await AsyncStorage.getItem("userToken");
      api.put("/users/password", user, {
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
          Authorization: "Bearer " + userToken
      }
      })
      .then(response => {
        console.log('response', JSON.stringify(response))
        DropDownHolder.getDropDown().alertWithType('success', i18n.t('message'), i18n.t('accountModified'));
        this.props.navigation.navigate("HomeScreen");

      })
      .catch(error => {
        console.log('error', JSON.stringify(error))
        const data =  error.response.data;
        DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message))
      });
    }
  }
  render() {
    let self = this;
    function gotToHome() {
      self.props.navigation.navigate("HomeScreen");
    }
    return (
      <ScrollView style={{ padding: 20 }}>
        <TextField
          placeholder={i18n.t('form.oldPassword')}
          name='oldPassword'
          security={true}
          onChangeText={this.onChangeText}
          onBlur={this.validateInput}
          error={this.state.errorForm.password}/>
        <TextField
          placeholder={i18n.t('form.password')}
          name='password'
          security={true}
          onChangeText={this.onChangeText}
          onBlur={this.validateInput}
          error={this.state.errorForm.password}/>
        <TextField
          placeholder={i18n.t('form.confirmPassword')}
          name='confirmPassword'
          security={true}
          onChangeText={this.onChangeText}
          onBlur={this.validateInput}
          error={this.state.errorForm.confirmPassword}/>
          
        <View style={{ margin: 7 }} />
        <Button title={i18n.t('validate')} color="#FF0066" onPress={() => this.modify()} />
      </ScrollView>

    );
  }
}

const mapStateToProps = state => {
  //console.log("stateOverView", state);
  const { helpers } = state;
  return { helpers };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: action => {
      dispatch(action);
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChangePasswordScreen);
