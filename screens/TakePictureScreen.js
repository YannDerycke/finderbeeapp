import React, { Component } from "react";
import { connect } from "react-redux";
import { ScrollView, Text, TextInput, View, Button, Image } from "react-native";
import {
  ImagePicker,
  Permissions,
  ImageManipulator,
  FileSystem,
  Vibration
} from "expo";
import api from "../utils/api";

class TakePictureScreen extends Component {
  static navigationOptions = {
    title: "Step2"
  };
  state = {
    CloudinaryPicture:
      "https://facebook.github.io/react-native/docs/assets/favicon.png",
    CurrentPicture:
      "https://facebook.github.io/react-native/docs/assets/favicon.png"
  };

  componentWillMount() {
    // console.log("this.propsPict", this.props);
    this.setState(
      {
        CurrentPicture: this.props.currentThing.picture
      },
      function() {}
    );
  }

  render() {
    let self = this;
    function savePicture(result) {
      // console.log("resultPOic", result);
      // ImagePicker saves the taken photo to disk and returns a local URI to it
      let localUri = result.uri;
      let filename = localUri.split("/").pop();

      // Infer the type of the image
      let match = /\.(\w+)$/.exec(filename);
      let type = match ? `image/${match[1]}` : `image`;

      // Upload the image using the fetch and FormData APIs
      let formData = new FormData();
      // Assume "photo" is the name of the form field the server expects
      formData.append("photo", {
        uri: localUri,
        name: filename,
        type
      });
      //console.log("self", self.props);
      formData.append("name", self.props.currentThing.name);
      formData.append("description", self.props.currentThing.description);
      const location = {
        latitude: self.props.currentThing.latitude,
        longitude: self.props.currentThing.longitude
      };
      formData.append("geo", JSON.stringify(self.props.currentThing.geo));
      formData.append("location", JSON.stringify(location));
      // console.log("formData", formData);

      api
        .post("/pictures/upload/1", formData, {
          headers: {
            Accept: "application/json",
            "content-type": "multipart/form-data",
            Authorization:
              "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Inlhbm4iLCJpYXQiOjE1NTcxNDc1MDh9.XO4N-5-6JfcFlzx2i0xDzdabOjAdL3bTL5Dvfw0bSSc"
          }
        })
        .then(response => {
          self.setState({
            CloudinaryPicture: response.data.rows[0].picture
          });
        })
        .catch(error => {
          console.log(error);
        });
    }
    async function takeAndUploadPhotoAsync() {
      await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL);
      let result = await ImagePicker.launchCameraAsync({
        allowsEditing: true,
        aspect: [4, 3]
      });
      let resizedPhoto = await ImageManipulator.manipulateAsync(
        result.uri,
        [
          {
            resize: {
              width: 600,
              height: (result.height / result.width) * 600
            }
          }
        ],
        {
          compress: 0.8,
          format: "jpg",
          base64: false
        }
      );
      // FileSystem.moveAsync({
      //   from: resizedPhoto.uri,
      //   to: `${FileSystem.documentDirectory}photos/Photo_id.jpg`
      // });
      console.log("resized", resizedPhoto);
      if (result.cancelled) {
        return;
      }
      self.setState({
        CurrentPicture: resizedPhoto.uri
      });
      //TO DELTE
      const action = {
        type: "ADD_PICTURE",
        value: resizedPhoto.uri
      };
      self.props.dispatch(action);
      //
      savePicture(resizedPhoto);
    }
    async function uploadPhotoAsync() {
      await Permissions.askAsync(Permissions.CAMERA_ROLL);
      let result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 3]
      });
      let resizedPhoto = await ImageManipulator.manipulateAsync(
        result.uri,
        [
          {
            resize: {
              width: 600,
              height: (result.height / result.width) * 600
            }
          }
        ],
        {
          compress: 0.8,
          format: "jpg",
          base64: false
        }
      );
      // FileSystem.moveAsync({
      //   from: resizedPhoto.uri,
      //   to: `${FileSystem.documentDirectory}photos/Photo_id.jpg`
      // });
      if (result.cancelled) {
        return;
      }
      self.setState({
        CurrentPicture: resizedPhoto.uri
      });
      //TO DELTE
      const action = {
        type: "ADD_PICTURE",
        value: resizedPhoto.uri
      };
      self.props.dispatch(action);
      //
      savePicture(resizedPhoto);
    }
    return (
      <ScrollView style={{ padding: 20 }}>
        <View style={{ margin: 7 }} />
        <Text>{JSON.stringify(this.props)}</Text>
        <Image
          style={{ width: 200, height: 150 }}
          source={{
            uri: this.state.CloudinaryPicture
          }}
        />
        <Image
          style={{ width: 200, height: 150 }}
          source={{
            uri: this.state.CurrentPicture
          }}
        />
        <Button
          title="Take Picture"
          onPress={() => takeAndUploadPhotoAsync()}
        />
        <Button title="Gallery" onPress={() => uploadPhotoAsync()} />
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  //console.log("state", state);
  const { currentThing } = state;
  return { currentThing };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: action => {
      dispatch(action);
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TakePictureScreen);
