import React, { Component } from "react";
import { LinearGradient } from 'expo'
import { connect } from "react-redux";
import { Ionicons } from "@expo/vector-icons";
import {
  ScrollView,
  Text,
  TextInput,
  View,
  Button,
  StyleSheet,
  AsyncStorage,
  Picker,
  ListView,
  TouchableHighlight,
  Image,
  Dimensions,
  RefreshControl,
  Animated
  
} from "react-native";
import moment from "moment";
import api from "../utils/api";
import DropDownHolder from "../utils/alert";
import i18n from '../utils/i18n';
import Header from "../components/Header"
import Images from '../assets/requireIcons';
import {getMyThings} from "../services/things";
import FilterModal from "./FilterModalScreen";

// const GradientHeader = props => (
//   <View style={{ backgroundColor: '#eee' }}>
//       <LinearGradient
//         colors={['red', 'blue']}
//         style={[StyleSheet.absoluteFill, { height: Header.HEIGHT }]}
//       >
//         <Header {...props} />
//       </LinearGradient>
//     </View>
//   )

class MyObjectsScreen extends React.Component {

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params } = navigation.state;
    const dimensions = Dimensions.get('window');
    return {
      // header: null,
      //headerTitle: "My Objects",

      // headerBackground: (
      //   <LinearGradient
      //     colors={['#F85479', '#F79F6D', '#F8C25E', '#579D9B', '#579D9B']}
      //     style={{ flex: 1 }}
      //     start={{x: 0, y: 0}}
      //     end={{x: 1, y: 0}}
      //   />
      // ),
      // headerStyle: {
      //   backgroundColor: "green"
      // },
      headerRight: (

        <View style={{flex:1, flexDirection:'row', padding:10}}>
          <Ionicons
            color={"#FF0066"}
            name="ios-funnel"
            style={{marginRight:5}}
            onPress={() => params.showFilter()}
            size={30}
          />
        </View>
      )

    };
  };
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      myThings: [],
      myThingsFiltered:[],
      refreshing: false,
      categories: [],
      types: [],
      categoryId: "",
      typeSelected:"",
      categorySelected:"",
      filterModalVisible: false
    };
  }

  componentDidMount() {
    console.log('inMyObject didmount')
    const { navigation } = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      console.log('difocus')
      const currentThing = navigation.getParam('thing');
      const type = navigation.getParam('type');
      console.log('currentThing', currentThing)
      console.log('type', type)

    });
    this.setState({categories: [...this.state.categories, ...this.props.helpers.categories]})
    this.setState({types: [...this.state.types, ...this.props.helpers.types]})
    this.props.navigation.setParams({ 
      showFilter: this.showFilter.bind(this)
    });

    if(navigation.getParam('force')) {
      this.getObjects(true);
    } else {
      this.getObjects();
    }
   
  }

  selectCategory = (categoryId) => {
    //this.props.navigation.setParams({ categoryId: categoryId });
    this.setState({categorySelected: categoryId})
    function filterByCategory(thing) {
      return ((thing.categoryId === categoryId || categoryId === "")  && (this.state.typeSelected === thing.typeId || this.state.typeSelected === ""));
    }
    const filtered = this.state.myThings.filter(filterByCategory.bind(this))
    this.setState({
      myThingsFiltered: filtered,
      countFiltered: filtered.length
    })
  };

  selectType = (typeId) => {
    //this.props.navigation.setParams({ typeId: typeId });
    this.setState({typeSelected: typeId})
    function filterByType(thing) {
      return ((thing.typeId === typeId || typeId === "")  && (this.state.categorySelected === thing.categoryId || this.state.categorySelected === ""));
    }
    const filtered = this.state.myThings.filter(filterByType.bind(this))
    this.setState({
      myThingsFiltered: filtered,
      countFiltered: filtered.length
    })
  };

  showFilter() {
    this.setState({filterModalVisible: true})
  }
  closeFilter() {
    this.setState({filterModalVisible: false})
  }

  


  async getObjects(force) {
    
    let self = this;
    console.log('force', force)
    const userToken = await AsyncStorage.getItem("userToken");
    //console.log('things, token', userToken)
    if (this.props.thingsState.myThings && !force) {
      this.props.dispatch({
        type: "SPINNER",
        value: false
      });
      self.setState({
        myThings: this.props.thingsState.myThings || [],
        myThingsFiltered: this.props.thingsState.myThings || []
      })
      return
    }

    try {
      this.props.dispatch({
        type: "SPINNER",
        value: true
      });
      console.log('force', force)
      api
        .get("/things/mine", {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + userToken
          }
        })
        .then(response => {
          this.props.dispatch({
            type: "SPINNER",
            value: false
          });
          //console.log("response.data", response.data[0]);
          const things = response.data;
          this.props.dispatch({
            type: "MY_THINGS",
            value: things
          });
          self.setState(
            { loading:false,
              refreshing: false,
              countFiltered: things.length,
              myThings: things,
              myThingsFiltered: things,
              categorySelected:"",
              typeSelected:""
            },
            function(e) {
              //console.log("callback set", e);
            }
          );
        })
        .catch(error => {
          console.log(error);
          const data =  error.response.data;
          DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message));
        });
    } catch (error) {
      console.error(error);
      this.props.dispatch({
        type: "SPINNER",
        value: false
      });
      const data =  error.response.data;
      DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message));
    }
  }

  goToDetails(row) {
    // row.date = moment(row.date).utc().format("DD-MM-YYYY");
    console.log('row my object', row)
    // const action = {
    //   type: "SELECT_OBJECT",
    //   value: {...row}
    // };
    // this.props.dispatch(action);
    this.props.navigation.navigate("AddScreen", {sort: "UPDATE", from:"myObject", thing: {...row}, onGoBack: () => this.getObjects(true), });
  }
  refreshControl() {
    this.getObjects(true)

  }
  render() {
    const { loading } = this.state;
    // if (loading) {
    //   return null;
    // }
    const dimensions = Dimensions.get('window');
    const imageHeight = Math.round(dimensions.width * 9 / 16);
    const imageWidth = dimensions.width - 20;
    
    return (

      <View style={{flex: 1}}>
        <FilterModal
          visible={this.state.filterModalVisible} 
          close={this.closeFilter.bind(this)}
          selectCategory={this.selectCategory.bind(this)} 
          selectType={this.selectType.bind(this)}
          typeId={this.state.typeSelected}
          categoryId={this.state.categorySelected}
          categories={this.state.categories}
          types={this.state.types}
          countResult={this.state.countFiltered}
        />
        <ScrollView 
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.refreshControl.bind(this)}
            />
          }
          scrollEventThrottle={16}
          onScroll = { Animated.event([
              {
                  nativeEvent: {
                      contentOffset: {
                          y: this.scrollY
                      }
                  }
              }
          ])}
        >
          <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap'}}>
            {this.state.myThingsFiltered.map(function (item, index) { 
              //console.log('item', item.name)
              //console.log('item', item)
              //  const {type, Poster, Title} = item;
              let type = item.type ? item.type.name : "noName";
              let category = item.category ? item.category.name : "noName";
              return (
              //key is the index of the array 
              //item is the single item of the array
              <View key={index}>
                <TouchableHighlight  onPress={() => this.goToDetails(item)}>
                  <View>
                    <Image
                      style={{ width:  (dimensions.width/2)-10, height: 200, margin:5 }}
                      source={{
                        uri:
                        item.picture === "azerty"
                            ? "https://via.placeholder.com/70x70.jpg"
                            : item.picture
                      }}
                    />
                    
                    <Image
                      style={{ width:  40,  height: 40, position: "absolute", left: 5, top: 5 }}
                      source={Images[item.category.name]}
                    />
                     {item.verified ?   <Ionicons
                      style={{ position: "absolute", right: 5, top: 5 }}
                      name="md-checkmark-circle-outline"
                      color= "green"
                      // name={focused ? "ios-list" : "ios-list-box"}
                      size={40} 
                      ios-hourglass
                    /> : null }
                    {!item.verified ?   <Ionicons
                      style={{ position: "absolute", right: 5, top: 5 }}
                      name="ios-hourglass"
                      color= "red"
                      // name={focused ? "ios-list" : "ios-list-box"}
                      size={40} 
                      
                    /> : null } 
                    {/* <Text>{item.user.surname}</Text>
                    <Text>{item.user.name}}</Text> */}
                    {/* <Text>{item.name}</Text> */}
                    {/* <Text>{item.category.name}</Text> */}
                    {/* <Text>{item.geo.coordinates[0]}</Text>
                    <Text>{item.geo.coordinates[1]}</Text>
                    <Text>{type}</Text>
                    <Text>{category}</Text> */}
                    {/* <Text>{item.category.name}</Text> */}
                    {/* <Text style={styles.text}>{item.description}</Text> */}
                  </View>
                </TouchableHighlight>
              </View>
            )}.bind(this))}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  item: {

  },
  listItem: {
    flex: 1,
    flexDirection: "row",
    borderBottomWidth: 1,
    borderBottomColor: "#d6d7da",
    padding: 6
  },
  icon: {
    position: "absolute",

  },
  
  imageWrapper: {
    padding: 5
  },
  title: {
    fontSize: 20,
    textAlign: "left",
    margin: 6
  },
  subtitle: {
    fontSize: 10,
    textAlign: "left",
    margin: 6
  }
});

const mapStateToProps = state => {
  //console.log("state", state);
  const { thingsState, helpers } = state;
  return { thingsState, helpers};
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: action => {
      dispatch(action);
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyObjectsScreen);
