import React, { Component } from "react";
import {
  ScrollView,
  Text,
  TextInput, 
  View,
  Button,
  StyleSheet,
  AsyncStorage
} from "react-native";
import DropDownHolder from "../utils/alert";
import TextField from '../components/TextField'
import {validate, validateAll, comparePassword} from "../utils/validate";
import i18n from '../utils/i18n';
import api from "../utils/api";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  input: {
    backgroundColor: '#42A5F5',
    margin: 10,
    padding: 8,
    color: 'white',
    borderRadius: 14,
    fontSize: 18,
    fontWeight: '500',
  }
});
class SignInScreen extends React.Component {
  // static navigationOptions = ({ navigation, navigationOptions }) => {
  //   const { params = {} } = navigation.state;
  //   return {
  //     headerTitle: params.title,
  //   };
  // };
  constructor(props) {
    console.log('bjjbbkbkb')
    super(props)
    this.state = {user:  {verified:false}, errorForm: {name:"", surname:"", email:"", password:""}}

  }
  componentDidMount() {  
    // const { navigation } = this.props;
    // console.log('nav', navigation)
    // navigation.setParams({ title: i18n.t('signIn') });
  }
  goBack = () => {
    this.props.navigation.navigate("HomeScreen");
  }
  onChangeText = (property, value) => { 
    this.setState({ user: { ...this.state.user, [property]: value} });
  }
  validateInput = (property) => {
    // let errorValue;
    // errorValue = validate(property, this.state.user[property]);
    // this.setState({
    //   errorForm: {...this.state.errorForm, [property]: errorValue}
    // })
  }
  signIn = () => {
    const user = this.state.user; 
    console.log('user', user)
    const testForm = validateAll(user, 'signIn');
    console.log('testForm', testForm)
    this.setState({
      errorForm: testForm === null ? {} : testForm
    })
    let validForm = !testForm;
    console.log('validForm', validForm)
    if(validForm) {
      api.post("/users/signIn", user, {
        headers: {
          Accept: "application/json",
          "Content-type": "application/json"
        }
      })
      .then(async response => {
        //console.log('response', JSON.stringify(response))
        const data =  response.data;
        AsyncStorage.setItem("userToken", data.token);
        const userToken = await AsyncStorage.getItem("userToken");
        console.log('userToken', userToken)
        // console.log('data', data)
        //this.dropDownAlertRef.alertWithType('success', i18n.t('message'), i18n.t(data.message));
        DropDownHolder.getDropDown().alertWithType('success', i18n.t('message'), i18n.t(data.message));
        this.props.navigation.navigate("App");
      })
      .catch(error => {
        const data =  error.response.data;
        // console.log('data', data)
        // this.dropDownAlertRef.alertWithType('error', i18n.t('message'), i18n.t(data.message));
        DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message));
      });
    }
  }
  render() {
    return (
      <ScrollView style={{ padding: 20 }}>
        <TextField
          name='email'
          placeholder={i18n.t('form.email')}
          // autoFocus={true}
          keyboardType={"email-address"}
          onChangeText={this.onChangeText}
          onBlur={this.validateInput}
          error={this.state.errorForm.email}/>
        <TextField
          name='password'
          placeholder={i18n.t('form.password')}
          // keyboardType={"visible-password"}
          security={true}
          onChangeText={this.onChangeText}
          onBlur={this.validateInput}
          error={this.state.errorForm.password}/>         
        <View style={{ margin: 7 }} />
        <Button color="#FF0066" title={i18n.t('signIn')} onPress={() => this.signIn()} />
      </ScrollView>

    );
  }
}

export default SignInScreen;
