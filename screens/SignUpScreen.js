import React, { Component } from "react";
import { connect } from "react-redux";
import {
  ScrollView,
  Text,
  TextInput, 
  View,
  Button,
  StyleSheet,
  Picker
} from "react-native";
import TextField from '../components/TextField'
import {validate, validateAll, comparePassword} from "../utils/validate";
import DropDownHolder from "../utils/alert";
import i18n from '../utils/i18n';
import api from "../utils/api";
// import App from "../App"


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  input: {
    backgroundColor: '#42A5F5',
    margin: 10,
    padding: 8,
    color: 'white',
    borderRadius: 14,
    fontSize: 18,
    fontWeight: '500',
  }
});
class SignUpScreen extends React.Component {
  // static navigationOptions = ({ navigation, navigationOptions }) => {
  //   const { params = {} } = navigation.state;
  //   return {
  //     headerTitle: params.title,
  //   };
  // };
  constructor(props) {
    super(props)
    this.state = {user:  {verified:false}, errorForm: {name:"", surname:"", email:"", password:"", language: i18n.locale}}
  }
  componentDidMount() {  
    // const { navigation } = this.props;
    // console.log('nav', navigation)
    // navigation.setParams({ title: i18n.t('signUp') });

    this.setState({language: i18n.locale})
    
  }
  goBack = () => {
    this.props.navigation.navigate("HomeScreen");
  }
  onChangeText = (property, value) => { 
    this.setState({ user: { ...this.state.user, [property]: value} });
    console.log('thisstate', this.state)
  }
  validateInput = (property) => {
    // let errorValue;
    // errorValue = validate(property, this.state.user[property]);
    // this.setState({
    //   errorForm: {...this.state.errorForm, [property]: errorValue}
    // })
  }
  async changeLanguage (language) {
    console.log('changeLanguage', language)
    this.setState({ user: {...this.state.user, language} });
    this.setState({language})
    // await AsyncStorage.setItem("language", language);
    console.log('thisstate', this.state)
  }
  signUp = () => {
    const user = this.state.user; 
    user.pushnotification = this.props.helpers.pushToken
    console.log('user', user)
    const testForm = validateAll(user, 'signUp');
    this.setState({
      errorForm: testForm === null ? {} : testForm
    })
    let validForm = !testForm;
    // console.log('user', user)
    if(validForm) {
      api.post("/users/signUp", user, {
        headers: {
          Accept: "application/json",
          "Content-type": "application/json"
      }
      })
      .then(response => {
        console.log('response', JSON.stringify(response))
        DropDownHolder.getDropDown().alertWithType('success', i18n.t('message'), i18n.t('accountCreated'));
        this.props.navigation.navigate("AuthScreen");

      })
      .catch(error => {
        console.log('error', JSON.stringify(error))
        const data =  error.response.data;
        DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message))
      });
    }
  }
  render() {
    let self = this;
    function gotToHome() {
      self.props.navigation.navigate("HomeScreen");
    }
    return (
      <ScrollView style={{ padding: 20 }}>
        <TextField
          name='name'
          placeholder={i18n.t('form.name')}
          // autoFocus={true}
          onChangeText={this.onChangeText}
          onBlur={this.validateInput}
          error={this.state.errorForm.name}/>
        <TextField
          placeholder={i18n.t('form.surName')}
          name='surname' 
          onChangeText={this.onChangeText}
          onBlur={this.validateInput}
          error={this.state.errorForm.surname}/>
        <View style={{flex:.4}}>
          <Picker style={{flex:1}}  selectedValue = {this.state.language}  onValueChange = {(value) => {this.changeLanguage(value)}}>
            <Picker.Item label="English" value="en"/>
            <Picker.Item label="Français" value="fr"/>
            <Picker.Item label="Español" value="es"/>
          </Picker>     
        </View> 
        <TextField
          placeholder={i18n.t('form.email')}
          name='email'
          onChangeText={this.onChangeText}
          onBlur={this.validateInput}
          error={this.state.errorForm.email}/>
        <TextField
          placeholder={i18n.t('form.password')}
          name='password'
          security={true}
          onChangeText={this.onChangeText}
          onBlur={this.validateInput}
          error={this.state.errorForm.password}/>
        <TextField
          placeholder={i18n.t('form.confirmPassword')}
          name='confirmPassword'
          security={true}
          onChangeText={this.onChangeText}
          onBlur={this.validateInput}
          error={this.state.errorForm.confirmPassword}/>
          
        <View style={{ margin: 7 }} />
        <Button title={i18n.t('signUp')} color="#FF0066" onPress={() => this.signUp()} />
      </ScrollView>

    );
  }
}

const mapStateToProps = state => {
  //console.log("stateOverView", state);
  const { helpers } = state;
  return { helpers };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: action => {
      dispatch(action);
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUpScreen);
