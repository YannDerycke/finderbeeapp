import React from "react";
import { connect } from "react-redux";
import { Ionicons } from "@expo/vector-icons";
import { 
  Picker, 
  AsyncStorage, 
  Text, 
  View, 
  Button, 
  Image, 
  Icon, 
  StyleSheet, 
  Dimensions, 
  Alert, 
  TextInput, 
  KeyboardAvoidingView,
  ScrollView } from "react-native";
import { Linking } from "expo";
import api from "../utils/api";
import {validate, validateAll, comparePassword} from "../utils/validate";
import Modal from "./ModalScreen";
import ModalMap from "./MapScreen";
import { StackNavigator } from "react-navigation";
import DatePicker from 'react-native-datepicker';
import moment from "moment";
import DropDownHolder from "../utils/alert";
import i18n from '../utils/i18n';

import {takeAndUploadPhotoAsync, uploadPhotoAsync, saveAll, deleteThing} from "../services/takePictures"

class AddThingScreen extends React.Component {
  static navigationOptions = ({ navigation, navigationOptions }) => {
    //const { params } = navigation.state;
    const { params = {} } = navigation.state;
    //console.log('params', params)
    return {
      headerStyle: {
        backgroundColor: "green",
      },
      headerRight: (
        
        <View style={{flex:1, flexDirection:'row', padding:10}}>
          {params.thing ? 
          <Ionicons
            color={"#FF0066"}
            name="md-trash"
            style={{marginRight:20}}
            onPress={() => params.erase()}
            size={30}
          />
            : null}
         {/* {params.fromMyThings ?  */}
            <Ionicons
            color={"#FF0066"}
            name="ios-save"
            style={{marginRight:5}}
            onPress={() => params.save()}
            size={30}
          />
           {/* : null}  */}
          {/* <Button
            onPress={() => params.save()}
            title="fdsfdsfdsfdsq"
            color="red"
          /> */}
          
        </View>
      )};
  };

  constructor(props) {
    super(props);
    
  }

  state = {
    picture: {},
    modalVisible: false,
    categories: [],
    types: [],
    currentThing: { 
    },
    init: {
      name:"Nom de l'objet",
      description: "Ce texte a pour autre avantage d'utiliser des mots de longueur variable, essayant de simuler une occupation normale. La méthode simpliste consistant à copier-coller un court texte plusieurs fois (« ceci est un faux-texte ",
      categoryId: 1,
      typeId :2,
      picture: "https://res.cloudinary.com/harbsvj1d/image/upload/v1567689295/finderBee/86/0e6ba4e1-44e1-4e00-8e90-cde3da29af8b.jpg",
      geo: {
        coordinates: [4.349705, 50.844803]
      },
      radius:1,
      reward:10,
      category: {},
      date: new Date()
    }
  };
  // setModalVisible(visible) {
  //   console.log('visible', visible);
  //   console.log('this', this)
  //   this.setState({modalVisible: visible});
  // }
  fuck() {
    console.log('bjbjbjbjb')
  }
  async componentDidMount() {
    const { navigation } = this.props;
    //console.log('nav', navigation)
    this.focusListener = navigation.addListener('didFocus', () => {

    });
    navigation.setParams({ launch: this.fuck.bind(this) })
    const sort = navigation.getParam('sort');
    const fromNavigation = navigation.getParam('from');
    // console.log('fromMyThings', fromMyThings)
    navigation.setParams({ save: this.save.bind(this) });
    navigation.setParams({ erase: this.confirmErase.bind(this) });
    const currentThing = navigation.getParam('thing');
    console.log('currentThing', currentThing)
   
    if(currentThing) {
      console.log('current', currentThing)
      currentThing.date = moment(currentThing.date).utc().format("DD-MM-YYYY");
      let showSlider;
      if(currentThing.type.name === "LOST") {
        showSlider = true;
      }
      this.setState({currentThing, showSlider})
    } else {
      this.setState({currentThing: this.state.init})
    }
    // navigation.setParams({ fromMyThings: this.erase.bind(this) });
    
    console.log('type sort', sort)
    
    this.setState({sort, fromNavigation})
    // this.setState({fromMyThings})
    let self = this;
    //console.log("this.propsOver", this.props);
    const userToken = await AsyncStorage.getItem("userToken");
    this.props.helpers.types.map(function (type) {
      if(type.name === "LOST") {
        this.setState(state => {
          state.currentThing.typeId = type.id;
          state.currentThing.type = type;
          state.showSlider = true
          return state
        })
      }
    }.bind(this))
  }

  takePicture() {
    takeAndUploadPhotoAsync().then((resizedPhoto)=>{
      console.log('resizedPhoto1', resizedPhoto)
      this.setState(state => {
        state.currentThing.picture = resizedPhoto.uri
        return state
      })
    }).catch((error)=>{
      console.log('error', error)
    });
  }

  uploadPicture() {
    uploadPhotoAsync().then((resizedPhoto)=>{
      console.log('resizedPhoto2', resizedPhoto)
      this.setState(state => {
        state.currentThing.picture = resizedPhoto.uri
        return state
      })
    }).catch((error)=>{ 
      console.log('error', error)
    });
  }
  
  chooseTypePicture() {
    const title = 'Choose type of Picture !';
    const message = 'Please make your selection.';
    const buttons = [
        { text: 'Cancel', type: 'cancel' },
        { text: 'Picture', onPress: () => this.takePicture() },
        { text: 'Library', onPress: () => this.uploadPicture()}
    ];
    Alert.alert(title, message, buttons);
  }

  confirmErase() {
    Alert.alert(
      i18n.t('confirmDelete'),
      "",
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => this.erase()},
      ],
      {cancelable: false},
    );
    console.log('errase', this.state.currentThing.id)
  }

  erase() {
    this.props.dispatch({
      type: "SPINNER",
      value: true
    });
    deleteThing(this.state.currentThing).then(response => {
      this.props.dispatch({
        type: "SPINNER",
        value: false
      });
      // this.props.dispatch({
      //   type,
      //   value: this.props.currentThing
      // });
      const data =  response.data;
      DropDownHolder.getDropDown().alertWithType('success', i18n.t('message'), i18n.t(data.message));
      console.log('deleted')
      this.props.navigation.state.params.onGoBack();
      this.props.navigation.goBack()
    }).catch(error => {
      this.props.dispatch({
        type: "SPINNER",
        value: false
      });
      console.log('error fuck erase',  JSON.stringify(error));
      DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t("error"));
    });
  }
  save() {
    const testForm = validateAll(this.state.currentThing, 'details');
    let validForm = !testForm;
    console.log('testForm', testForm)
    if(testForm) {
      const title = 'ERROR MISSING DATA !';
      let message = ""
      const values = Object.values(testForm)
      //console.log('vlaies', values)
      values.forEach(val => {
        message = message + `\n ${val}  ${i18n.t('empty')}`
      });
      // const message = 'Please ADD MISSING DATA.';
  
      Alert.alert(title, message);
      //console.log('testFOrm', testForm);
      //console.log('validForm', validForm);
    } else {
      this.props.dispatch({
        type: "SPINNER",
        value: true
      });
      const sort  = this.state.sort;
      // const id = this.props.currentThing.id;
      saveAll.call(this, this.state.currentThing).then(response => {
        this.setState({currentThing:this.state.init})
        this.props.dispatch({
          type: "SPINNER",
          value: false
        });
        const data =  response.data;
        console.log('responseUpload', JSON.stringify(response))
        // this.setState({
        //   CloudinaryPicture: response.data.rows[0].picture
        // });
        DropDownHolder.getDropDown().alertWithType('success', i18n.t('message'), i18n.t(data.message));
        
        if (this.props.navigation.state.params.onGoBack) {
          this.props.navigation.state.params.onGoBack();
          this.props.navigation.goBack()
        } else {
          this.props.navigation.navigate("MyObjectsScreen", {force: true})
        }
        console.log('this.props.naviation', this.props.navigation)  
      })
      .catch(error => {
        console.log('error fuckd',  JSON.stringify(error));
        this.props.dispatch({
          type: "SPINNER",
          value: false
        });
        DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t("error"));
      });
    }
  }
  selectGeo(infos) {
    // console.log('infos', infos)
    // this.setState(state => {
    //   state.currentThing.geo = infos.geo;
    //   state.currentThing.radius = infos.radius;
    //   return state
    // })
  }

  changeType(value) {
    console.log('value', value)
    this.props.helpers.types.map(function (type) {
      console.log('type', type)
      if(type.id === value) {
        this.setState(state => {
          state.currentThing.typeId = type.id;
          state.currentThing.type = type;
          return state
        })
        if(type.originName === "FOUND") {
          console.log('innnnn')
          this.setState(state => {
            state.currentThing.radius = 1;
            state.showSlider = false
            return state
          })
        } else {
          this.setState(state => {
            state.currentThing.radius = 100;
            state.showSlider = true
            return state
          })
        }
      }
      
    }.bind(this))

    this.setState({modalVisible: true})
  }

  sendInfos(infosMap) {
    console.log('infosMap', infosMap)
    this.setState(state => {
      state.currentThing.geo = infosMap.geo;
      state.currentThing.radius = infosMap.radius;
      state.modalVisible = false;
      return state
    })
  }

  toggleModal(visible) {
    this.setState({ modalVisible: visible });
  }


  render() {
    const dimensions = Dimensions.get('window');
    const imageHeight = Math.round(dimensions.width * 3 / 4);
    const imageWidth = dimensions.width;
    
    if ( this.state.categories.length === 0 && this.state.types.length ) {
      return null;
    }
      return (
        <KeyboardAvoidingView
          style={{ flex: 1, flexDirection: 'column',justifyContent: 'center',}} 
          behavior="padding" 
          enabled   
          keyboardVerticalOffset={100}
        >
          <ScrollView style={{  flex: 1, padding: 0 }}>

            {/* <View style={{ flex: 1, flexDirection:'column'}}> */}
            <View style={{flex:1, marginBottom:15, flexDirection:'row'}}>
              <View style={{flex:1}}>
                <Picker 
                  selectedValue = {this.state.currentThing.typeId} 
                  onValueChange = {(value) => {this.changeType(value)}}
                >
                {this.props.helpers.types.reduce( (filtered, type)=>{
                  if (typeof(type.id) === "number") {
                    filtered.push(<Picker.Item label={type.name} value={type.id} key={type.id} />)
                  }
                  return filtered;
                  }, [])}
                </Picker>
              </View>
                
              <View style={{flex:1}}>
                <Picker 
                  selectedValue = {this.state.currentThing.categoryId} 
                  onValueChange = {(value) => this.setState(state => {
                    state.currentThing.categoryId = value;
                    return state
                  })}
                  >
                {this.props.helpers.categories.reduce( (filtered, category)=>{
                  if (typeof(category.id) === "number") {
                    filtered.push(<Picker.Item label={category.name} value={category.id} key={category.id} />)
                  }
                  return filtered;
                  }, [])}
                </Picker>
              </View>
            </View>           
            <View style={{flex:4, flexDirection:'column'}}>
              {/* <Text>{this.props.currentThing.radius}</Text> */}
              <Image
                style={{ width: imageWidth, height: imageHeight}}
                source={{
                  uri: this.state.currentThing.picture
                }}
              />
              <Ionicons
                style={styles.map}
                color={"#FF0066"}
                name="md-pin"
                // name={focused ? "ios-home" : "md-home"}
                onPress={() => this.toggleModal(true)}
                size={40}
              />
              <Ionicons
                style={styles.takePicture}
                color={"#FF0066"}
                name="ios-camera"
                onPress={() => this.chooseTypePicture("TakePictureScreen")}
                size={40}
              />
              {this.state.modalVisible ?
              <ModalMap 
                currentThing={this.state.currentThing} 
                selectGeo={this.selectGeo.bind(this)}
                sendInfos={this.sendInfos.bind(this)}
                modalVisible={this.state.modalVisible}
                showSlider={this.state.showSlider}
              />: null }
            </View>
            <View style={{ flex: 1, padding:5, flexDirection:'column'}}>
              <View style={{flex:5, marginBottom:15, flexDirection:'row'}}>
                  <DatePicker
                    style={{flex: .5}}
                    date={this.state.currentThing.date} //initial date from state
                    mode="date" //The enum of date, datetime and time
                    placeholder="select date"
                    format="DD-MM-YYYY"
                    minDate="01-01-2016"
                    maxDate= {new Date()}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        left: 0,
                        top: 4,
                        marginLeft: 0
                      },
                      dateInput: {
                        marginLeft: 36
                      }
                    }}
                    onDateChange={
                      (date) => {      
                        this.setState(state => {
                        state.currentThing.date = date
                        return state
                      }); console.log('date2', moment(date,"DD-MM-YYYY "))}}
                  />
              </View>
              <View style={{ flex: 1 }}>
                  <Text style={styles.title}>{i18n.t('addScreen.title')}</Text> 
                  <TextInput
                    style={styles.input}
                    placeholder={i18n.t('addScreen.title')}
                    onChangeText={(text) => this.setState(state => {
                      state.currentThing.name = text
                      return state
                    })}
                    value={this.state.currentThing.name}
                  />
                  {/* <Text>{JSON.stringify(this.state.currentThing.date)}</Text> */}
                  <Text style={styles.title}>{i18n.t('addScreen.description')}</Text>
                  <TextInput
                    style={[styles.input]}
                    multiline={true}
                    numberOfLines={4}
                    placeholder={i18n.t('addScreen.description')}
                    onBlur={() => {this.setState({descriptionInput: true})}}
                    onChangeText={(text) => this.setState(state => {
                      state.currentThing.description = text
                      return state
                    })}
                    value={this.state.currentThing.description}
                  />
                  <Text style={styles.title}>{i18n.t('addScreen.reward')}</Text>
                  <View style={{flexDirection:'row'}}>
                    <TextInput 
                      style={[styles.input]}
                      keyboardType='numeric'
                      onChangeText={(text) => this.setState(state => {
                        state.currentThing.reward = text
                        return state
                      })}
                      value={`${this.state.currentThing.reward}`}
                      maxLength={10}  //setting limit of input
                    />
                    <Text style={[styles.input, {textAlignVertical: 'center' }]}>{i18n.t('addScreen.eur')}</Text>
                  </View>
              </View>

            </View>
            {/* <Text>{JSON.stringify(this.props)}</Text> */}

          {/* </View> */}
          </ScrollView>
        </KeyboardAvoidingView>
      );
    }
}

const mapStateToProps = state => {
  //console.log("stateOverView", state);
  const { thingsState, helpers } = state;
  return { thingsState, helpers };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: action => {
      dispatch(action);
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddThingScreen);

const styles = StyleSheet.create({
  takePicture: {
    position: "absolute",
    right: 5,
    bottom: 0
  },
  title: {
    fontSize: 18,
    color: 'black'
  },
  map: {
    position: "absolute",
    left: 5,
    bottom: 0
  },
  input: {
    margin: 10,
    padding: 8,
    backgroundColor: '#FF0066',
    color: 'white',
    borderRadius: 14,
    fontSize: 16,
    fontWeight: '500',
  }
});
