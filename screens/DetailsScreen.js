import React from "react";
import { connect } from "react-redux";
import { Ionicons } from "@expo/vector-icons";
import { 
  Picker, 
  AsyncStorage, 
  Text, 
  View, 
  Button, 
  Image, 
  Icon, 
  StyleSheet, 
  Dimensions, 
  Alert, 
  TextInput, 
  KeyboardAvoidingView,
  ScrollView } from "react-native";
import { NavigationActions } from 'react-navigation';
import { Linking, Constants} from "expo";
import api from "../utils/api";
import {validate, validateAll, comparePassword} from "../utils/validate";
import Modal from "./ModalScreen";
import ModalMap from "./MapScreen";
import { StackNavigator } from "react-navigation";
import DatePicker from 'react-native-datepicker';
import moment from "moment";
import DropDownHolder from "../utils/alert";
import i18n from '../utils/i18n';

import {takeAndUploadPhotoAsync, uploadPhotoAsync, saveAll, deleteThing} from "../services/takePictures"

class DetailsScreen extends React.Component {
  static navigationOptions = ({ navigation, navigationOptions }) => {
    //console.log('navigationOptions', navigationOptions)
    //const { params } = navigation.state;
    const { params = {} } = navigation.state;
    //console.log('params', params)
    //console.log('navigation', navigation)
    return {
      headerTitle: navigation.state.params.title,
      headerStyle: {
        backgroundColor: "green",
        headerStyle: { marginTop: Constants.statusBarHeight }
      }
    };
  };

  constructor(props) {
    super(props);
    
  }

  state = {
    picture: {},
    modalVisible: false,
    categories: [],
    types: [],
    currentThing: {
      type:'',
      category:'',
    }
  };
  // setModalVisible(visible) {
  //   console.log('visible', visible);
  //   console.log('this', this)
  //   this.setState({modalVisible: visible});
  // }
  async componentDidMount() { 
    // let x = ()=> {
    //   console.log('fuck')
    //   const backAction = NavigationActions.back({
    //     routName: 'MyObjectsScreen',
    //   });
    //   this.props.navigation.dispatch(backAction);
    // }

    // this.props.navigation.actions.goBack = x.bind(this);
    // console.log('this.props.navigation1', this.props.navigation.actions.goBack)
    
    Linking.getInitialURL().then((url) => {
      if (url) {
        const { navigation } = this.props;
        // console.log('Initial url is: ' + url);
        // console.log('this.props.navigation', this.props.navigation)
        
      }
    }).catch(err => console.error('An error occurred', err));

    const { navigation } = this.props;
    const currentThing = navigation.getParam('thing');
    this.setState({currentThing})
    //navigation.setParams({ title: currentThing.name })
    const userToken = await AsyncStorage.getItem("userToken");
    //console.log('things, token', userToken)
    const thingId = this.props.navigation.getParam('id');
    console.log('thingId', thingId)
    if (thingId) {
      try {
        api
          .get(`/things/find/${thingId}`, {
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + userToken
            }
          })
          .then(response => {
            const data =  response.data;
            console.log("response.data cate", data);
            this.setState({currentThing:data})
        
          })
          .catch(error => {
            console.log(error);
            const data =  error.response.data;
            DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message));
          });
      } catch (error) {
        console.error(error);
        const data =  error.response.data;
        DropDownHolder.getDropDown().alertWithType('error', i18n.t('message'), i18n.t(data.message));
      }
    }
  }
  selectGeo(infos) {
    console.log('infos', infos)
  }
  sendInfos(infosMap) {
    console.log('infosMap', infosMap)
    this.setState(state => {
      state.modalVisible = false;
      return state
    })
  }

  toggleModal(visible) {
    this.setState({ modalVisible: visible });
  }
  render() {
    const dimensions = Dimensions.get('window');
    const imageHeight = Math.round(dimensions.width * 3 / 4);
    if ( this.state.categories.length === 0 && this.state.types.length ) {
      return null;
    }
      return (
        <KeyboardAvoidingView
          style={{ flex: 1, flexDirection: 'column',justifyContent: 'center',}} 
          behavior="padding" 
          enabled   
          keyboardVerticalOffset={100}
        >
        <ScrollView style={{ padding: 5 }}>
            {this.state.currentThing && this.state.currentThing.geo ? <View style={{ flex: 1 }}>
                        
                        <View style={{flex: 1 }}>
                          {/* <Text>{this.props.currentThing.radius}</Text> */}
                          <Image
                            style={{ width: dimensions.width, height: imageHeight }}
                            source={{
                              uri: this.state.currentThing.picture
                            }}
                          />
                          <Ionicons
                            style={styles.map}
                            color={"#FF0066"}
                            name="md-pin"
                            // name={focused ? "ios-home" : "md-home"}
                            onPress={() => this.toggleModal(true)}
                            size={40}
                          />
                          {this.state.modalVisible ?
                          <ModalMap 
                            currentThing={this.state.currentThing} 
                            selectGeo={this.selectGeo.bind(this)}
                            sendInfos={this.sendInfos.bind(this)}
                            modalVisible={this.state.modalVisible}
                            mode={"VIEW"}
                          />: null }
                        </View>
                        <View style={{ flex: 1, padding:5, flexDirection:'column'}}>
                            <Text style={styles.title}>{i18n.t('addScreen.title')}</Text> 
                            <Text style={styles.info}>{this.state.currentThing.name} </Text>
                            <Text style={styles.title}>{i18n.t('addScreen.description')}</Text>
                            <Text style={styles.info}>{this.state.currentThing.description} </Text>
                            <Text style={styles.title}>{i18n.t('addScreen.reward')}</Text>
                            <View style={{flexDirection:'row'}}>
                              <Text style={styles.info}>{this.state.currentThing.reward} eur </Text>
                              {/* <Text style={{textAlignVertical: 'center' }}>{i18n.t('addScreen.eur')}</Text> */}
                            </View>
                            
                            {/* <Text>{this.state.currentThing.type.name} </Text>
                            <Text>{this.state.currentThing.category.name} </Text>
                            <Text>{this.state.currentThing.date} </Text> */}
                          {/* <Button
                            title="Save"
                            style={{flex:1}}
                            onPress={() => this.save()}
                          /> */}
                        </View>
                        {/* <Text>{JSON.stringify(this.state.currentThing)}</Text> */}
          
                      </View>: null}
            
            </ScrollView>
          </KeyboardAvoidingView>

      );
    }
}

const mapStateToProps = state => {
  //console.log("stateOverView", state);
  const { thingsState, helpers } = state;
  return { thingsState, helpers };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: action => {
      dispatch(action);
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailsScreen);

const styles = StyleSheet.create({

  map: {
    position: "absolute",
    left: 5,
    bottom: 0
  },
  info: {
    backgroundColor: '#FF0066',
    margin: 10,
    padding: 8,
    color: 'white',
    borderRadius: 14,
    fontSize: 18,
    fontWeight: '500',
  },
  title: {
    fontSize: 18,
    color: 'black'
  },
});
