// Store/configureStore.js
import { combineReducers } from "redux";

import { createStore } from "redux";
import { currentThing } from "./reducers/form";
import { thingsState } from "./reducers/thingsState";
import { helpers } from "./reducers/helpers";

export default createStore(combineReducers(
    {
        currentThing, 
        thingsState, 
        helpers
    }
));
