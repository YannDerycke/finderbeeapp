import { combineReducers } from "redux";

const initialState = {
  // myThings:[],
  // myThingsFiltered:[],
  things: [],
  thingsFiltered: [],
  categoryFilter: "",
  typeFilter: "",
};

export function thingsState(state = initialState, action) {
  //console.log("actionVal", action);
  let nextState;
  switch (action.type) {

    case "MY_THINGS": 
      nextState = {
        ...state,
        myThings: action.value,
      };
      //console.log("nexState", nextState);
      return nextState;

      case "EMPTY_MY_THINGS": 
      nextState = {
        ...state,
        myThings: [],
      };
      //console.log("nexState", nextState);
      return nextState;
    case "LIST_THINGS":
        //console.log('action', action)
      nextState = {
        ...state,
        things: action.value.things,
        thingsFiltered: action.value.things,
        countFiltered: action.value.things.length
        // categoryFilter: action.value.categoryFilter,
        // typeFilter: action.value.typeFilter

      };
      //console.log("nexState", nextState);
      return nextState;

    case "FILTER_TYPE":
      // let filter = action.value;
      console.log('action.value', action.value)
        function filterByType(thing) {
          //console.log('thing.typeId === action.value', thing.typeId === action.value)
          return ((thing.typeId === action.value || action.value === "") && (state.categoryFilter === thing.categoryId || state.categoryFilter === ""));
        }
      const filteredByType = state.things.filter(filterByType);
  
      nextState = {
        ...state,
        thingsFiltered: [...filteredByType],
        typeFilter : action.value,
        countFiltered: filteredByType.length
      };
      //console.log("nexStateFILETER BY", nextState);
      return nextState;
    case "FILTER_CATEGORY":
        // let filter = action.value;
        // console.log('action.value CAt', action.value)
        // console.log('state.typeFilter', state.typeFilter)
        function filterByCategory(thing) {
          // console.log('thing type', thing.typeId)
          // console.log('thing.categoryId === action.value', thing.categoryId === action.value)
          // console.log('state.typeFilter === thing.typeId ', state.typeFilter === thing.typeId )
          // console.log('thing.typeId === action.value', thing.typeId === action.value)
          return ((thing.categoryId === action.value || action.value === "")  && (state.typeFilter === thing.typeId || state.typeFilter === ""));
        }
        const filteredByCategory = state.things.filter(filterByCategory);
        
        nextState = {
          ...state,
          thingsFiltered: [...filteredByCategory], 
          categoryFilter : action.value,
          countFiltered: filteredByCategory.length
        };
        //console.log("nexStateFILETER BY", nextState);
        return nextState;

    default:
      return state;
  }
}

// export default combineReducers({
//   myThings: myThings
// });