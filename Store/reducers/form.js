import { combineReducers } from "redux";
import moment from "moment";

const initialState = {
  name:"test",
  description: "test",
  categoryId: 3,
  typeId :2,
  picture: "https://res.cloudinary.com/harbsvj1d/image/upload/v1567008751/finderBee/86/a08d4f12-97a1-4068-bf67-37d6944211a9.jpg",
  geo: {
    coordinates: [3.9986888319253917,
      50.00027518604269]
  },
  radius:1,
  type: {},
  category: {},
  date: new Date()
};

export function currentThing(state = initialState, action) {
  //console.log("actionVal", action);
  let nextState;
  switch (action.type) {
    case "CREATE":
      nextState = initialState;
      //console.log("nexState", nextState);
      return nextState;
    case "SELECT_OBJECT":
      action.value.date = moment(action.value.date).utc().format("DD-MM-YYYY");
      nextState = action.value;
      //console.log("nexState", nextState);
      return nextState;
    case "ADD_NAME":
        //console.log("action.value NAME", action.value);
      nextState = {
        ...state,
        name: action.value
      };
     // console.log("nexStateName", nextState);
      return nextState;
    case "ADD_TYPE":
        console.log("action.value TYPE", action.value);
      nextState = {
        ...state,
        typeId: action.value
      };
      console.log("nexStateType", nextState);
      return nextState;
      case "ADD_CATEGORY":
        console.log("action.value Category", action.value);
      nextState = {
        ...state,
        categoryId: action.value
      };
      console.log("nexStateType", nextState);
      return nextState;
    case "ADD_DESCRIPTION":
      nextState = {
        ...state,
        description: action.value
      };
     // console.log("nexState", nextState);
      return nextState;
    case "ADD_LOCATION":
      console.log('action addLocation', action.value)
      const newStates = Object.assign({}, state);
      newStates.geo = {coordinates : [action.value.long,  action.value.lat]};
      newStates.radius = action.value.radius;
      nextState = newStates;
      console.log("nexState", nextState);
      return nextState;

    case "ADD_PICTURE":
      console.log('addPic', action.value)
      nextState = {
        ...state,
        picture: action.value
      };
      //console.log("nexState", nextState);
      return nextState;
    case "ADD_DATE":
        console.log('ADD_DATE', action.value)
        nextState = {
          ...state,
          date: action.value
        };
        console.log("nexState", nextState);
        return nextState;

    default:
      return state;
  }
}

// export default combineReducers({
//   currentThing: addObject
// });
