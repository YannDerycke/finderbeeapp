import { combineReducers } from "redux";

const initialState = {
  spinner: false,
  categories: [],
  types: [],
  user: {
    id:86,
    name:"yann",
    surname:"derycke"
  },
  pushToken:""
};

export function helpers(state = initialState, action) {
  //console.log("actionVal", action);
  let nextState;
  switch (action.type) {

    case "USER":
        //console.log("action.value NAME", action.value);
      nextState = {
        ...state,
        user: action.value
      };
      // console.log("nexStateName", nextState);
      return nextState;
    case "PUSH_TOKEN":
        console.log("action.value PUSH_TOKEN", action.value);
      nextState = {
        ...state,
        pushToken: action.value
      };
      // console.log("nexStateName", nextState);
      return nextState; 
    case "SPINNER":
        //console.log("action.value NAME", action.value);
      nextState = {
        ...state,
        spinner: action.value
      };
     // console.log("nexStateName", nextState);
      return nextState;
    case "ADD_CATEGORIES":
        //console.log("action.value NAME", action.value);
      nextState = {
        ...state,
        categories: action.value
      };
     // console.log("nexStateName", nextState);
      return nextState;
      case "ADD_TYPES":
        //console.log("action.value NAME", action.value);
      nextState = {
        ...state,
        types: action.value
      };
     // console.log("nexStateName", nextState);
      return nextState;
    default:
      return state;
  }
}

// export default combineReducers({
//   currentThing: addObject
// });
